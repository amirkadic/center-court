/*******************************************************************************
 * Copyright (c) 2012 Amir Kadić.
 * 
 * This file is part of Center Court.
 * 
 * Center Court is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Center Court is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Center Court.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Contributors:
 *     Amir Kadić - initial API and implementation
 ******************************************************************************/
package tennis.web.serialization;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.thoughtworks.xstream.converters.basic.AbstractSingleValueConverter;

public class DateConverter extends AbstractSingleValueConverter{

	private static final DateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
	
	
	@Override
	public String toString(Object obj) {
		return format.format((Date)obj);
	}

	@Override
	public boolean canConvert(Class klass) {
		return Date.class.equals(klass);
	}

	@Override
	public Object fromString(String string) {
		try {
			return format.parse(string);
		} catch (ParseException e) {
			throw new RuntimeException();
		}
	}

}
