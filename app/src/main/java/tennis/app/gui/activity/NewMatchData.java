/*******************************************************************************
 * Copyright (c) 2012 Amir Kadić.
 * 
 * This file is part of Center Court.
 * 
 * Center Court is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Center Court is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Center Court.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Contributors:
 *     Amir Kadić - initial API and implementation
 ******************************************************************************/
package tennis.app.gui.activity;

import java.util.ArrayList;
import java.util.List;

import org.springframework.util.Assert;

import tennis.app.gui.MatchScoreListener;
import tennis.app.gui.dialog.TargetScoreDialog;
import tennis.db.SqliteMatchStore;
import tennis.db.SqlitePlayerStore;
import tennis.domain.IllegalMoveException;
import tennis.domain.IllegalScoreException;
import tennis.domain.MatchFactory;
import tennis.domain.Surface;
import tennis.domain.entity.Match;
import tennis.domain.entity.Player;
import tennis.domain.value.ScoreInSets;
import tennis.storage.MatchStore;
import tennis.storage.PlayerStore;
import tennis.storage.StorageException;
import tennis.app.R;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class NewMatchData extends Activity {

	private static final int TARGET_SCORE_DIALOG = 1;
	private static final int NOTE_DIALOG = 2;
	private static final int SERVING_FIRST_DIALOG = 3;
	private MatchStore matchStore;
	private MatchFactory matchFactory;
	private Surface surface = Surface.HARD;
	private ScoreInSets initialScore;
	private PlayerStore playerStore;
	private String note;
	private TargetScoreDialog targetScoreDialog;
	private MatchScoreListener initialScoreSetter = new MatchScoreListener() {

		public void notify(ScoreInSets matchScore) throws IllegalScoreException {
			if (matchScore == null) {
				return;
			}
			if (matchScore.canFollow(ScoreInSets.INITIAL_SCORE)) {
				initialScore = matchScore;
				Log.d("NewMatchData", "Selected initial score of "
						+ initialScore);
				// FIXME text setting double coded and hard coded here
				((TextView) findViewById(R.id.initialScoreTextView))
						.setText("Initial score (" + initialScore.toString()
								+ ")");
			} else {
				throw new IllegalScoreException("Invalid target score.");
			}

		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.new_match);
		// player name selection
		Spinner playerNameSpinner = (Spinner) findViewById(R.id.playerSpinner);
		playerNameSpinner
				.setOnItemSelectedListener(new OnItemSelectedListener() {

					public void onItemSelected(AdapterView<?> parent,
							View view, int position, long id) {
						Player player = (Player) parent.getAdapter().getItem(
								position);
						((EditText) findViewById(R.id.playerNameEditText))
								.setText(player.getName());
					}

					public void onNothingSelected(AdapterView<?> parent) {
					}
				});
		// opponent name selection
		Spinner opponentNameSpinner = (Spinner) findViewById(R.id.opponentSpinner);
		opponentNameSpinner
				.setOnItemSelectedListener(new OnItemSelectedListener() {

					public void onItemSelected(AdapterView<?> parent,
							View view, int position, long id) {
						Player opponent = (Player) parent.getAdapter().getItem(
								position);
						((EditText) findViewById(R.id.opponentNameEditText))
								.setText(opponent.getName());
					}

					public void onNothingSelected(AdapterView<?> parent) {
					}
				});
		// start match button
		findViewById(R.id.startMatchButton).setOnClickListener(
				new View.OnClickListener() {

					public void onClick(View view) {
						NewMatchData.this.askWhoIsServingFirst();
					}
				});
		findViewById(R.id.initialScoreButton).setOnClickListener(
				new View.OnClickListener() {

					public void onClick(View view) {
						showDialog(TARGET_SCORE_DIALOG);
					}
				});
		((Spinner) findViewById(R.id.courtSurfaceSpinner))
				.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

					public void onItemSelected(AdapterView<?> parent,
							View view, int position, long id) {
						NewMatchData.this.surface = Surface
								.fromValue((String) parent.getAdapter()
										.getItem(position));
						Assert.notNull(NewMatchData.this.surface);
					}

					public void onNothingSelected(AdapterView<?> parent) {
					}
				});
		// note button
		findViewById(R.id.noteButton).setOnClickListener(
				new View.OnClickListener() {

					public void onClick(View view) {
						showDialog(NOTE_DIALOG);
					}
				});
	}

	@Override
	protected Dialog onCreateDialog(int id) {
		if (id == TARGET_SCORE_DIALOG) {
			Log.d("NewMatchData", "Creating target score dialog.");
			this.targetScoreDialog = new TargetScoreDialog(this,
					initialScoreSetter);
			return this.targetScoreDialog.getDialog();
		} else if (id == NOTE_DIALOG) {
			return this.createNoteDialog("About this match", "");
		} else if (id == SERVING_FIRST_DIALOG) {
			return this.createServingFirstDialog();
		} else {

			return null;
		}
	}

	@Override
	protected void onPrepareDialog(int id, Dialog dialog) {
		if (id == TARGET_SCORE_DIALOG) {
			this.targetScoreDialog.prepare(initialScore);
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		this.matchFactory = new MatchFactory();
		this.matchStore = SqliteMatchStore
				.getSingleton(getApplicationContext());
		this.playerStore = SqlitePlayerStore
				.getSingelton(getApplicationContext());
		// initializing player name spinner
		Spinner playerNameSpinner = (Spinner) findViewById(R.id.playerSpinner);
		ArrayAdapter<Player> playersAdapter = null;
		List<Player> allPlayers = new ArrayList<Player>();
		try {
			allPlayers = this.playerStore.allPlayers();
		} catch (StorageException e) {
			Log.e("NewMatchData", "Couldn't retrieve players from repository.",
					e);
		}
		playersAdapter = new ArrayAdapter<Player>(this,
				android.R.layout.simple_spinner_item, allPlayers);
		playersAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		playerNameSpinner.setAdapter(playersAdapter);
		// // initializing opponent name spinner
		Spinner opponentNameSpinner = (Spinner) findViewById(R.id.opponentSpinner);
		opponentNameSpinner.setAdapter(playersAdapter);
		((EditText) findViewById(R.id.playerNameEditText)).setText("");
		((EditText) findViewById(R.id.opponentNameEditText)).setText("");
	}

	protected void askWhoIsServingFirst() {
		showDialog(SERVING_FIRST_DIALOG);
	}

	protected void startTheMatch() {
		Match match = this.matchFactory.startNewMatch();
		match.setSurface(this.surface);
		match.getPlayer().setName(
				((EditText) findViewById(R.id.playerNameEditText)).getText()
						.toString());
		match.getOpponent().setName(
				((EditText) findViewById(R.id.opponentNameEditText)).getText()
						.toString());
		if (initialScore != null) {
			try {
				match.fastForwardTo(initialScore);
			} catch (IllegalMoveException e) {
				Log.e("NewMatchData",
						"Illegal initial score, not tracking this match.", e);
				shortToast("Illegal score.");
				return;
			}
		}
		if (this.note != null) {
			match.setNote(this.note);
		}
		try {
			match.readyForTracking();
		} catch (IllegalStateException e) {
			shortToast(e.getMessage());
			return;
		}
		try {
			this.matchStore.put(match);
		} catch (StorageException e) {
			Log.e("NewMatchData",
					"Couldn't persist the new match, not gonna track it.", e);
			return;
		}

		Intent trackingIntent = new Intent(this.getApplicationContext(),
				Tracking.class);
		trackingIntent.putExtra(Constants.MATCH_ID, match.getId());
		startActivity(trackingIntent);
	}

	private void shortToast(String message) {
		Toast toast = Toast.makeText(getApplicationContext(), message,
				Toast.LENGTH_SHORT);
		toast.show();
	}

	private Dialog createNoteDialog(String title, String prompt) {
		final Dialog dialog = new Dialog(this);
		dialog.setContentView(R.layout.note);
		dialog.setTitle(title);
		((TextView) dialog.findViewById(R.id.commentPrompt)).setText(prompt);
		EditText commentEditText = (EditText) dialog
				.findViewById(R.id.commentfield);
		commentEditText.setText(NewMatchData.this.note);

		Button ok = (Button) dialog.findViewById(R.id.noteOKButton);
		ok.setOnClickListener(new OnClickListener() {

			public void onClick(View arg0) {
				EditText commentEditText = (EditText) dialog
						.findViewById(R.id.commentfield);
				NewMatchData.this.note = commentEditText.getText().toString();

				dialog.dismiss();
			}
		});
		Button cancelButton = (Button) dialog
				.findViewById(R.id.noteCancelButton);
		cancelButton.setOnClickListener(new OnClickListener() {

			public void onClick(View arg0) {
				dialog.cancel();
			}
		});

		return dialog;

	}

	private Dialog createServingFirstDialog() {
		final Dialog dialog = new Dialog(this);
		dialog.setTitle("Who' serving first?");
		Rect displayRectangle = new Rect();
		Window window = getWindow();
		window.getDecorView().getWindowVisibleDisplayFrame(displayRectangle);

		// inflate and adjust layout
		LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View layout = inflater.inflate(R.layout.serving_first, null);
		layout.setMinimumWidth((int) (displayRectangle.width() * 0.9f));
		// layout.setMinimumHeight((int) (displayRectangle.height() * 0.9f));
		dialog.setContentView(layout);
		Button ok = (Button) dialog.findViewById(R.id.playerServingFirstButton);
		ok.setOnClickListener(new OnClickListener() {

			public void onClick(View arg0) {

				dialog.dismiss();
				startTheMatch();
			}
		});
		Button cancelButton = (Button) dialog
				.findViewById(R.id.opponentServingFirstButton);
		cancelButton.setOnClickListener(new OnClickListener() {

			public void onClick(View arg0) {
				dialog.dismiss();
				startTheMatch();
			}
		});

		return dialog;
	}

}
