/*******************************************************************************
 * Copyright (c) 2012 Amir Kadić.
 * 
 * This file is part of Center Court.
 * 
 * Center Court is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Center Court is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Center Court.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Contributors:
 *     Amir Kadić - initial API and implementation
 ******************************************************************************/
package tennis.domain.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import tennis.domain.IllegalMoveException;
import tennis.domain.MatchFactory;
import tennis.domain.PointFactory;
import tennis.domain.WinStatus;
import tennis.domain.entity.CountableScore;
import tennis.domain.entity.Match;
import tennis.domain.entity.Point;
import tennis.domain.memento.MatchMemento;
import tennis.domain.value.MatchScore;
import tennis.domain.value.ScoreInSets;

public class DefaultTrackingService implements TrackingService {

	private static DefaultTrackingService singleton;
	private MatchFactory matchFactory = new MatchFactory();
	private PointFactory pointFactory = new PointFactory();
	private Match match;
	private List<MatchMemento> matchMementos = new ArrayList<MatchMemento>();

	private DefaultTrackingService() {
	}

	public static TrackingService getTrackingService() {
		if (DefaultTrackingService.singleton == null) {
			DefaultTrackingService.singleton = new DefaultTrackingService();
		}
		return DefaultTrackingService.singleton;
	}

	public void start() {
		this.matchMementos.clear();
		this.match = this.matchFactory.startNewMatch();
	}

	public void resume(Match match) {
		this.match = match;
		this.matchMementos.clear();
	}

	public void winPoint() throws IllegalMoveException {
		this.playPoint(this.pointFactory.wonPoint());
	}

	public void losePoint() throws IllegalMoveException {
		this.playPoint(this.pointFactory.lostPoint());
	}

	private void playPoint(Point point) throws IllegalMoveException {
		MatchMemento memento = this.match.saveToMemento();
		this.matchMementos.add(memento);
		this.match.add(point);
	}

	public boolean matchOver() {
		return this.match.getStatus() != WinStatus.PENDING;
	}

	public Match getMatch() {
		return this.match;
	}

	public MatchScore currentScore() {
		return this.match.currentScore();
	}

	public void stop() {
		this.matchMementos.clear();
		this.match = null;
	}

	public boolean isRunning() {
		return this.match != null;
	}

	public void undo() throws IllegalMoveException {
		this.match
				.restore(this.matchMementos.remove(this.matchMementos.size() - 1));
	}

	public boolean canUndo() {
		return !this.matchMementos.isEmpty();
	}

	public void jumpTo(ScoreInSets targetScore) throws IllegalMoveException {
		MatchMemento memento = this.match.saveToMemento();
		this.matchMementos.add(memento);
		this.match.fastForwardTo(targetScore);
	}

	public void commentLastPoint(String text) {

		Point currentPoint = this.match.getCurrentSet().getCurrentGame()
				.getCurrentPoint();
		if (currentPoint != null) {
			currentPoint.setComment(text);
		}
	}

	public void pause() {
		// not implemented as long as persistence is handled in the activity
		// Tracking.
		// after refactoring, this should persist the match and called in 2
		// cases:
		// 1) when the tracking is paused explicitly by user
		// 2) when the activity is paused (onPause())
		this.match = null;
	}

	public void finishTracking() {
		this.match.finishTracking();
		this.match = null;
	}

	public ScoreInSets currentScoreInSets() {
		return this.match.currentScore().getScoreInSets();
	}

}
