/*******************************************************************************
 * Copyright (c) 2012 Amir Kadić.
 * 
 * This file is part of Center Court.
 * 
 * Center Court is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Center Court is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Center Court.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Contributors:
 *     Amir Kadić - initial API and implementation
 ******************************************************************************/
package tennis.web.serialization;

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import org.junit.Test;

import tennis.domain.Handedness;
import tennis.domain.IllegalMoveException;
import tennis.domain.PointFactory;
import tennis.domain.SetFactory;
import tennis.domain.Surface;
import tennis.domain.entity.Match;
import tennis.domain.entity.Player;
import tennis.web.serialization.XmlMatchSerializer;

public class XmlMatchSerializerTest {
	private final static XmlMatchSerializer SERIALIZER = new XmlMatchSerializer();
	private final static PointFactory POINT_FACTORY = new PointFactory();

	@Test
	public void testNewMatch() throws IllegalMoveException, IOException {

		Player player = new Player();
		player.setAge(17);
		player.setHandedness(Handedness.RIGHT);
		player.setName("Boris Becker");
		player.setRanking("Junior");
		player.setSurface(Surface.GRASS);

		Player opponent = new Player();
		opponent.setAge(17);
		opponent.setHandedness(Handedness.RIGHT);
		opponent.setName("Kevin Curren");
		opponent.setRanking("ATP 5");
		opponent.setSurface(Surface.HARD);

		Match match = new Match();
		match.setPlayer(player);
		match.setOpponent(opponent);
		match.setSetFactory(new SetFactory());
		match.start();
		match.add(POINT_FACTORY.wonPoint());
		OutputStream os = new ByteArrayOutputStream();
		SERIALIZER.serialize(match, os);
		os.close();
	}

	@Test
	public void testTiebreak() throws IllegalMoveException, IOException {
		Player player = new Player();
		player.setAge(17);
		player.setHandedness(Handedness.RIGHT);
		player.setName("Boris Becker");
		player.setRanking("Junior");
		player.setSurface(Surface.GRASS);

		Player opponent = new Player();
		opponent.setAge(17);
		opponent.setHandedness(Handedness.RIGHT);
		opponent.setName("Kevin Curren");
		opponent.setRanking("ATP 5");
		opponent.setSurface(Surface.HARD);

		Match match = new Match();
		match.setPlayer(player);
		match.setOpponent(opponent);
		match.setSetFactory(new SetFactory());
		match.start();
		// win five games
		for (int i = 0; i < 20; i++) {
			match.add(POINT_FACTORY.wonPoint());
		}
		//lose six games
		for (int i = 0; i < 24; i++) {
			match.add(POINT_FACTORY.lostPoint());
		}
		// win one game to create a tiebreak
		for (int i = 0; i < 4; i++) {
			match.add(POINT_FACTORY.wonPoint());
		}
		// win the tiebreak
		for (int i = 0; i < 7; i++) {
			match.add(POINT_FACTORY.wonPoint());
		}
		OutputStream os = new ByteArrayOutputStream();
		SERIALIZER.serialize(match, os);
		os.close();
	}

}
