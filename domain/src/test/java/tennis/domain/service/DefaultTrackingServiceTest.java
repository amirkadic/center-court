/*******************************************************************************
 * Copyright (c) 2012 Amir Kadić.
 * 
 * This file is part of Center Court.
 * 
 * Center Court is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Center Court is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Center Court.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Contributors:
 *     Amir Kadić - initial API and implementation
 ******************************************************************************/
package tennis.domain.service;

import java.util.Arrays;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import tennis.domain.IllegalMoveException;
import tennis.domain.WinStatus;
import tennis.domain.entity.CountableScore;
import tennis.domain.entity.EndScore;
import tennis.domain.service.DefaultTrackingService;
import tennis.domain.value.BestOf3ScoreInSets;
import tennis.domain.value.MatchScore;
import tennis.domain.value.ScoreInSets;

public class DefaultTrackingServiceTest {

	private DefaultTrackingService service;

	@Before
	public void setup() {
		this.service = (DefaultTrackingService) DefaultTrackingService
				.getTrackingService();
	}

	@Test
	public void testCommentLastPoint() {
		this.service.start();
		this.service.commentLastPoint("Ace");
	}

	@Test
	public void straightWin() throws IllegalMoveException {

		this.service.start();
		Assert.assertFalse(this.service.canUndo());
		Assert.assertTrue(this.service.isRunning());

		ScoreInSets endOfFirstSet = new BestOf3ScoreInSets(
				Arrays.asList(new CountableScore(6, 0)));
		this.service.jumpTo(endOfFirstSet);
		Assert.assertTrue(this.service.canUndo());
		for (int i = 0; i < 24; i++) {
			this.service.winPoint();
		}
		Assert.assertTrue(this.service.canUndo());
		Assert.assertEquals(
				new MatchScore(new BestOf3ScoreInSets(Arrays.asList(
						new CountableScore(6, 0), new CountableScore(6, 0))),
						new EndScore()), this.service.currentScore());
		Assert.assertEquals(WinStatus.WON, this.service.getMatch().getStatus());
		Assert.assertEquals(new CountableScore(2, 0), this.service.getMatch()
				.getCurrentScore());
		this.service.undo();
		Assert.assertEquals(WinStatus.PENDING, this.service.getMatch()
				.getStatus());
		this.service.commentLastPoint("High shot");
		this.service.losePoint();
		this.service.winPoint();
		Assert.assertEquals(WinStatus.WON, this.service.getMatch().getStatus());
		Assert.assertTrue(this.service.matchOver());
		this.service.stop();
		Assert.assertFalse(this.service.isRunning());
	}

}
