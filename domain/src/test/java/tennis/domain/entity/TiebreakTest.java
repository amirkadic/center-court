/*******************************************************************************
 * Copyright (c) 2012 Amir Kadić.
 * 
 * This file is part of Center Court.
 * 
 * Center Court is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Center Court is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Center Court.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Contributors:
 *     Amir Kadić - initial API and implementation
 ******************************************************************************/
package tennis.domain.entity;

import java.util.Date;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import tennis.domain.GameFactory;
import tennis.domain.IllegalMoveException;
import tennis.domain.WinStatus;
import tennis.domain.entity.CountableScore;
import tennis.domain.entity.Point;
import tennis.domain.entity.Tiebreak;
import tennis.domain.memento.TiebreakMemento;

public class TiebreakTest {
	private static final GameFactory gameFactory = new GameFactory();

	@Before
	public void setup() {
	}
	
	@Test
	public void testAdd() throws IllegalMoveException {
		Tiebreak tiebreak = TiebreakTest.gameFactory.startTiebreak();
		Assert.assertEquals(WinStatus.PENDING, tiebreak.getStatus());
		Point point = new Point();
		point.setStatus(WinStatus.WON);
		point.setDate(new Date());
		tiebreak.add(point);
		Assert.assertEquals(new CountableScore(1, 0), tiebreak.getScore());
		Assert.assertEquals(new CountableScore(1, 0), point.getScore());
		Assert.assertEquals(WinStatus.PENDING, tiebreak.getStatus());
	}
	
	@Test(expected=IllegalMoveException.class)
	public void testTooManyPoints() throws IllegalMoveException {
		Tiebreak tiebreak = TiebreakTest.gameFactory.startTiebreak();
		for(int i=0; i<8; i++) {
			this.winPoint(tiebreak);
		}
	}
	
	@Test
	public void testMemento() throws IllegalMoveException {
		Tiebreak tiebreak = TiebreakTest.gameFactory.startTiebreak();
		Assert.assertEquals(WinStatus.PENDING, tiebreak.getStatus());
		// 1:0
		this.winPoint(tiebreak);
		Assert.assertEquals(new CountableScore(1, 0), tiebreak.getScore());
		TiebreakMemento memento = (TiebreakMemento) tiebreak.saveToMemento();
		// 2:0
		this.winPoint(tiebreak);
		Assert.assertEquals(new CountableScore(2, 0), tiebreak.getScore());
		// restore to 1:0
		tiebreak = (Tiebreak) memento.restore();
		Assert.assertEquals(new CountableScore(1, 0), tiebreak.getScore());
		// 2:0 again
		this.winPoint(tiebreak);
		// 3:0
		this.winPoint(tiebreak);
		// 4:0
		this.winPoint(tiebreak);		
		// 5:0
		this.winPoint(tiebreak);		
		// 6:0
		this.winPoint(tiebreak);		
		// 7:0
		this.winPoint(tiebreak);		
		Assert.assertEquals(WinStatus.WON, tiebreak.getStatus());
		// restore to 15:0
		tiebreak = (Tiebreak) memento.restore();
		Assert.assertEquals(new CountableScore(1, 0), tiebreak.getScore());
		Assert.assertEquals(WinStatus.PENDING, tiebreak.getStatus());

	}

	
	private void winPoint(Tiebreak tiebreak) throws IllegalMoveException {
		Point point = new Point();
		point.setStatus(WinStatus.WON);
		tiebreak.add(point);
		
	}

}
