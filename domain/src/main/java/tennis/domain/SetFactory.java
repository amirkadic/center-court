/*******************************************************************************
 * Copyright (c) 2012 Amir Kadić.
 * 
 * This file is part of Center Court.
 * 
 * Center Court is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Center Court is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Center Court.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Contributors:
 *     Amir Kadić - initial API and implementation
 ******************************************************************************/
package tennis.domain;

import java.util.ArrayList;
import java.util.List;

import tennis.domain.entity.CountableScore;
import tennis.domain.entity.EndScore;
import tennis.domain.entity.Score;
import tennis.domain.entity.Set;

public class SetFactory {

	private GameFactory gameFactory;

	public Set startNewSet() {
		Set set = new Set();
		set.setGameFactory(this.gameFactory);
		set.start();
		return set;
	}

	public List<Set> fromScores(List<CountableScore> setScores, Score gameScore)
			throws IllegalMoveException {
		List<Set> sets = new ArrayList<Set>();

		for (int i = 0; i < setScores.size(); i++) {
			Set set = this.startNewSet();
			set.fastForwardTo(setScores.get(i));
			sets.add(set);
		}
		return sets;
	}

	public void setGameFactory(GameFactory gameFactory) {
		this.gameFactory = gameFactory;
	}

}
