/*******************************************************************************
 * Copyright (c) 2012 Amir Kadić.
 * 
 * This file is part of Center Court.
 * 
 * Center Court is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Center Court is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Center Court.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Contributors:
 *     Amir Kadić - initial API and implementation
 ******************************************************************************/
package tennis.db;

import java.util.ArrayList;
import java.util.List;

import tennis.domain.entity.Player;
import tennis.storage.PlayerStore;
import tennis.storage.StorageException;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

/**
 * Sqlite based repository of {@link Player}s.
 * 
 * @author amir
 * 
 */
public class SqlitePlayerStore implements PlayerStore {

	private static SqlitePlayerStore singleton;

	private SQLiteDatabase readableDb;
	private SQLiteDatabase writableDb;

	/**
	 * Preventing instantiation from outside. Use
	 * {@link SqlitePlayerStore#getSingelton(Context)} instead.
	 */
	private SqlitePlayerStore() {
	}

	/**
	 * Factory method.
	 * 
	 * @param context
	 *            Android context.
	 * @return Singleton instance.
	 */
	public static PlayerStore getSingelton(Context context) {
		if (singleton == null) {
			singleton = new SqlitePlayerStore();
			OpenHelper openHelper = OpenHelper.getStore(context);
			singleton.setReadableDb(openHelper.getReadableDatabase());
			singleton.setWritableDb(openHelper.getWritableDatabase());
		}
		return singleton;
	}

	public void put(Player player) throws StorageException {
		ContentValues values = new ContentValues();
		// FIXME this is crude, try updating the row instead
		if (player.getId() != 0) {
			this.writableDb.delete(OpenHelper.PLAYER_TABLE,
					OpenHelper.PLAYER_COLUMN_ID + "=" + player.getId(), null);
			values.put(OpenHelper.PLAYER_COLUMN_ID, player.getId());
		}
		if (player.getName().length() > 100) {
			throw new StorageException("Player name too long.");
		}
		long id;
		// avoid inserting players with duplicate names
		Cursor sameNameCursor = this.readableDb.query(OpenHelper.PLAYER_TABLE,
				null, OpenHelper.PLAYER_COLUMN_NAME + "='" + player.getName()
						+ "'", null, null, null, null);
		if (sameNameCursor.moveToNext()) {
			id = sameNameCursor.getLong(0);
		} else {
			values.put(OpenHelper.PLAYER_COLUMN_NAME, player.getName());
			id = this.writableDb.insert(OpenHelper.PLAYER_TABLE, null, values);
		}
		player.setId(id);
	}

	public Player getPlayer(long id) throws StorageException {
		Cursor playerCursor = this.readableDb.query(OpenHelper.PLAYER_TABLE,
				null, OpenHelper.PLAYER_COLUMN_ID + "=" + id, null, null, null,
				null);
		if (playerCursor.getCount() > 1) {
			throw new StorageException("More than one player with id " + id
					+ " found.");
		}
		Player player = null;
		if (playerCursor.moveToNext()) {
			player = this.mapCursorToPlayer(playerCursor);
		}
		return player;
	}

	public List<Player> allPlayers() throws StorageException {
		List<Player> players = new ArrayList<Player>();
		Cursor playerCursor = this.readableDb.query(OpenHelper.PLAYER_TABLE,
				null, null, null, null, null, null);
		while (playerCursor.moveToNext()) {
			players.add(this.mapCursorToPlayer(playerCursor));
		}
		return players;
	}

	// setters

	public void setReadableDb(SQLiteDatabase readableDb) {
		this.readableDb = readableDb;
	}

	public void setWritableDb(SQLiteDatabase writableDb) {
		this.writableDb = writableDb;
	}

	// private methods

	private Player mapCursorToPlayer(Cursor playerCursor) {
		Player player = new Player();
		player.setId(playerCursor.getLong(0));
		player.setName(playerCursor.getString(1));
		return player;
	}

}
