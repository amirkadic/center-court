/*******************************************************************************
 * Copyright (c) 2012 Amir Kadić.
 * 
 * This file is part of Center Court.
 * 
 * Center Court is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Center Court is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Center Court.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Contributors:
 *     Amir Kadić - initial API and implementation
 ******************************************************************************/
package tennis.domain;

/**
 * When a state transition is impossible according to the rules of the current
 * type of match (e.g. adding a point to a game that is already finished).
 * 
 * @author amir
 * 
 */
public class IllegalMoveException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public IllegalMoveException(String message) {
		super(message);
	}

	private IllegalMoveException(String message, Throwable cause) {
		super(message, cause);
	}

	private IllegalMoveException(Throwable cause) {
		super(cause);
	}
	
	

}
