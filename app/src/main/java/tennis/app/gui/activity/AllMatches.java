/*******************************************************************************
 * Copyright (c) 2012 Amir Kadić.
 * 
 * This file is part of Center Court.
 * 
 * Center Court is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Center Court is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Center Court.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Contributors:
 *     Amir Kadić - initial API and implementation
 ******************************************************************************/
package tennis.app.gui.activity;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import tennis.app.gui.dto.ParcelableMatchReference;
import tennis.db.SqliteMatchStore;
import tennis.domain.entity.Match;
import tennis.storage.MatchStore;
import tennis.storage.StorageException;
import tennis.app.R;
import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class AllMatches extends ListActivity {

	private MatchStore matchStore;
	private ArrayAdapter<ParcelableMatchReference> listAdapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.allmatches);
		registerForContextMenu(findViewById(android.R.id.list));
		Button newMatchButton = (Button) findViewById(R.id.newMatchButton);
		newMatchButton.setOnClickListener(new View.OnClickListener() {

			public void onClick(View view) {
				trackNewMatch();
			}
		});
	}

	@Override
	protected void onResume() {
		super.onResume();
		this.matchStore = SqliteMatchStore.getSingleton(this
				.getApplicationContext());

		ParcelableMatchReference[] matchReferences;

		List<Match> allMatches = new ArrayList<Match>();
		// FIXME long-running: do in background
		try {
			allMatches = matchStore.allMatches();
		} catch (StorageException e) {
			Log.e("MarchReferencesProducer", e.getMessage());
			matchReferences = new ParcelableMatchReference[0];
		}
		matchReferences = new ParcelableMatchReference[allMatches.size()];
		int i = 0;
		for (Match match : allMatches) {
			String playerName = match.getPlayer().getName();
			if (playerName == null)
				playerName = "unknown";
			String opponentName = match.getOpponent().getName();
			if (opponentName == null)
				opponentName = "unknown";
			String label = playerName + " vs " + opponentName + ", "
					+ new Date(match.getTime());
			matchReferences[i++] = new ParcelableMatchReference(match.getId(),
					label);
		}
		// FIXME try to avoid instantiating a new adapter on every resume
		ArrayList<ParcelableMatchReference> list = new ArrayList<ParcelableMatchReference>();
		list.addAll(Arrays.asList(matchReferences));
		this.listAdapter = new ArrayAdapter<ParcelableMatchReference>(this,
				R.layout.matchlist_item, list);
		this.listAdapter.setNotifyOnChange(true);
		setListAdapter(this.listAdapter);

	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {
		AdapterContextMenuInfo contextMenuInfo = (AdapterContextMenuInfo) item
				.getMenuInfo();
		switch (item.getItemId()) {
		case R.id.resume:
			this.resumeMatch(AllMatches.getMatchReference(contextMenuInfo,
					getListView(), getListAdapter()).getId());
			return true;
		case R.id.delete: {
			this.shortToast("Deleting match");
			ParcelableMatchReference matchReference = AllMatches
					.getMatchReference(contextMenuInfo, getListView(),
							getListAdapter());
			// stop tracking if the match being tracked is about to be deleted
			// if (this.trackingService.isRunning()
			// && this.trackingService.getMatch().getTime() == matchReference
			// .getDate()) {
			// this.trackingService.stop();
			// }
			new DeleteMatchTask(this.matchStore, this.listAdapter)
					.execute(matchReference);
			return true;
		}
		case R.id.upload: {
			this.longToast("Uploading match");
			return super.onContextItemSelected(item);
			// ParcelableMatchReference matchReference = MatchList
			// .getMatchReference(contextMenuInfo, getListView(),
			// getListAdapter());
			// // stop tracking if the match being tracked is about to be
			// deleted
			// if (this.trackingService.isRunning()
			// && this.trackingService.getMatch().getTime() == matchReference
			// .getDate()) {
			// this.trackingService.stop();
			// }
			// new UploadTask(LocalMatchStore.getStore(getApplicationContext()),
			// (TextView) contextMenuInfo.targetView)
			// .execute(matchReference);
			// return true;
		}
		case R.id.delete_all:
			new DeleteAllMatchesTask(this.matchStore, this.listAdapter)
					.execute();
			return true;
		case R.id.upload_all:
			return true;
		default:
			return super.onContextItemSelected(item);
		}
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.match_ops, menu);
		// TODO disable resuming finished matches!
	}

	private void resumeMatch(long id) {
		Intent trackingIntent = new Intent(this.getApplicationContext(),
				Tracking.class);
		trackingIntent.putExtra(Constants.MATCH_ID, id);
		startActivity(trackingIntent);

	}

	private void shortToast(String message) {
		Toast toast = Toast.makeText(getApplicationContext(), message,
				Toast.LENGTH_SHORT);
		toast.show();
	}

	private void longToast(String message) {
		Toast toast = Toast.makeText(getApplicationContext(), message,
				Toast.LENGTH_LONG);
		toast.show();
	}

	static ParcelableMatchReference getMatchReference(
			AdapterContextMenuInfo contextMenuInfo, ListView listView,
			ListAdapter listAdapter) {
		int selectedMatchIndex = listView
				.indexOfChild(contextMenuInfo.targetView);
		return (ParcelableMatchReference) listAdapter
				.getItem(selectedMatchIndex);
	}

	protected void trackNewMatch() {

		Intent matchDataIntent = new Intent(this.getApplicationContext(),
				NewMatchData.class);
		startActivity(matchDataIntent);
	}

}
