/*******************************************************************************
 * Copyright (c) 2012 Amir Kadić.
 * 
 * This file is part of Center Court.
 * 
 * Center Court is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Center Court is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Center Court.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Contributors:
 *     Amir Kadić - initial API and implementation
 ******************************************************************************/
package tennis.app.gui.activity;

import java.util.ArrayList;
import java.util.List;

import tennis.app.gui.dto.ParcelableMatchReference;
import tennis.storage.MatchStore;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;

public class DeleteAllMatchesTask extends
		AsyncTask<Void, Void, List<ParcelableMatchReference>> {

	private MatchStore matchStore;
	private ArrayAdapter<ParcelableMatchReference> listAdapter;

	public DeleteAllMatchesTask(MatchStore matchStore,
			ArrayAdapter<ParcelableMatchReference> listAdapter) {
		super();
		this.matchStore = matchStore;
		this.listAdapter = listAdapter;
	}

	@Override
	protected List<ParcelableMatchReference> doInBackground(Void... v) {
		List<ParcelableMatchReference> deletedMatches = new ArrayList<ParcelableMatchReference>();
		for (int i = 0; i < this.listAdapter.getCount(); i++) {
			ParcelableMatchReference match = this.listAdapter.getItem(i);
			if (matchStore.remove(match.getId())) {
				Log.d("MatchList", "Deleted match " + match.getId() + ".");
				deletedMatches.add(match);
			}
		}
		return deletedMatches;
	}

	@Override
	protected void onPostExecute(List<ParcelableMatchReference> deletedMatches) {
		for (ParcelableMatchReference deletedMatch : deletedMatches) {
			listAdapter.remove(deletedMatch);
		}
	}

}
