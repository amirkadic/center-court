/*******************************************************************************
 * Copyright (c) 2012 Amir Kadić.
 * 
 * This file is part of Center Court.
 * 
 * Center Court is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Center Court is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Center Court.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Contributors:
 *     Amir Kadić - initial API and implementation
 ******************************************************************************/
package tennis.domain.entity;


import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import tennis.domain.IllegalMoveException;
import tennis.domain.PlayGraphFactory;
import tennis.domain.WinStatus;
import tennis.domain.entity.Advantage;
import tennis.domain.entity.CountableScore;
import tennis.domain.entity.Deuce;
import tennis.domain.entity.EndScore;
import tennis.domain.entity.PlayGraph;

public class PlayGraphTest {

	private static PlayGraphFactory playGraphFactory;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		playGraphFactory = new PlayGraphFactory();
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Test
	public void straightWin() throws IllegalMoveException {
		PlayGraph playGraph = playGraphFactory.newGameGraph();
		
		Assert.assertEquals(WinStatus.PENDING, playGraph.currentWinStatus());

		playGraph.move(WinStatus.WON);
		Assert.assertEquals(new CountableScore((short)15, (short)0), playGraph.currentPlayElement().getScore());
		Assert.assertEquals(WinStatus.PENDING, playGraph.currentWinStatus());

		playGraph.move(WinStatus.WON);
		Assert.assertEquals(WinStatus.PENDING, playGraph.currentWinStatus());
		Assert.assertEquals(new CountableScore((short)30, (short)0), playGraph.currentPlayElement().getScore());

		playGraph.move(WinStatus.WON);
		Assert.assertEquals(WinStatus.PENDING, playGraph.currentWinStatus());
		Assert.assertEquals(new CountableScore((short)40, (short)0), playGraph.currentPlayElement().getScore());

		playGraph.move(WinStatus.WON);
		Assert.assertEquals(WinStatus.WON, playGraph.currentWinStatus());
		Assert.assertTrue(playGraph.currentPlayElement().getScore().getClass().equals(EndScore.class));
	}

	@Test
	public void tightWin() throws IllegalMoveException {
		PlayGraph playGraph = playGraphFactory.newGameGraph();
		
		Assert.assertEquals(WinStatus.PENDING, playGraph.currentWinStatus());

		playGraph.move(WinStatus.WON);
		Assert.assertEquals(new CountableScore((short)15, (short)0), playGraph.currentPlayElement().getScore());
		Assert.assertEquals(WinStatus.PENDING, playGraph.currentWinStatus());

		playGraph.move(WinStatus.LOST);
		Assert.assertEquals(new CountableScore((short)15, (short)15), playGraph.currentPlayElement().getScore());
		Assert.assertEquals(WinStatus.PENDING, playGraph.currentWinStatus());

		playGraph.move(WinStatus.WON);
		Assert.assertEquals(WinStatus.PENDING, playGraph.currentWinStatus());
		Assert.assertEquals(new CountableScore((short)30, (short)15), playGraph.currentPlayElement().getScore());

		playGraph.move(WinStatus.LOST);
		Assert.assertEquals(new CountableScore((short)30, (short)30), playGraph.currentPlayElement().getScore());
		Assert.assertEquals(WinStatus.PENDING, playGraph.currentWinStatus());

		playGraph.move(WinStatus.WON);
		Assert.assertEquals(WinStatus.PENDING, playGraph.currentWinStatus());
		Assert.assertEquals(new CountableScore((short)40, (short)30), playGraph.currentPlayElement().getScore());

		// deuce
		// FIXME this is bad. a deuce state/score should be identifiable, and so should (dis)advantage
		// and game won/lost
		playGraph.move(WinStatus.LOST);
		Assert.assertTrue(playGraph.currentPlayElement().getScore().getClass().equals(Deuce.class));
		Assert.assertEquals(WinStatus.PENDING, playGraph.currentWinStatus());

		// advantage
		playGraph.move(WinStatus.WON);
		Assert.assertEquals(WinStatus.PENDING, playGraph.currentWinStatus());
		Assert.assertTrue(playGraph.currentPlayElement().getScore().getClass().equals(Advantage.class));

		// game won
		playGraph.move(WinStatus.WON);
		Assert.assertEquals(WinStatus.WON, playGraph.currentWinStatus());
		Assert.assertTrue(playGraph.currentPlayElement().getScore().getClass().equals(EndScore.class));

	}

	
	@Test
	public void straightLoss() throws IllegalMoveException {
		PlayGraph playGraph = playGraphFactory.newGameGraph();
		
		Assert.assertEquals(WinStatus.PENDING, playGraph.currentWinStatus());

		playGraph.move(WinStatus.LOST);
		Assert.assertEquals(new CountableScore((short)0, (short)15), playGraph.currentPlayElement().getScore());
		Assert.assertEquals(WinStatus.PENDING, playGraph.currentWinStatus());

		playGraph.move(WinStatus.LOST);
		Assert.assertEquals(WinStatus.PENDING, playGraph.currentWinStatus());
		Assert.assertEquals(new CountableScore((short)0, (short)30), playGraph.currentPlayElement().getScore());

		playGraph.move(WinStatus.LOST);
		Assert.assertEquals(WinStatus.PENDING, playGraph.currentWinStatus());
		Assert.assertEquals(new CountableScore((short)0, (short)40), playGraph.currentPlayElement().getScore());

		playGraph.move(WinStatus.LOST);
		Assert.assertEquals(WinStatus.LOST, playGraph.currentWinStatus());
		Assert.assertTrue(playGraph.currentPlayElement().getScore().getClass().equals(EndScore.class));
	}

	@Test(expected=IllegalMoveException.class)
	public void cannotPlayAPendantPoint() throws IllegalMoveException {
		PlayGraph playGraph = playGraphFactory.newGameGraph();
		
		Assert.assertEquals(WinStatus.PENDING, playGraph.currentWinStatus());

		playGraph.move(WinStatus.PENDING);
	}
	
	@Test(expected=IllegalMoveException.class)
	public void cannotPlayAPointInALostGame() throws IllegalMoveException {
		PlayGraph playGraph = playGraphFactory.newGameGraph();
		
		Assert.assertEquals(WinStatus.PENDING, playGraph.currentWinStatus());

		playGraph.move(WinStatus.LOST);
		playGraph.move(WinStatus.LOST);
		playGraph.move(WinStatus.LOST);
		playGraph.move(WinStatus.LOST);
		Assert.assertEquals(WinStatus.LOST, playGraph.currentWinStatus());
		Assert.assertTrue(playGraph.currentPlayElement().getScore().getClass().equals(EndScore.class));
		playGraph.move(WinStatus.WON);
	}

}
