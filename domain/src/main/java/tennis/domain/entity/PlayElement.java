/*******************************************************************************
 * Copyright (c) 2012 Amir Kadić.
 * 
 * This file is part of Center Court.
 * 
 * Center Court is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Center Court is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Center Court.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Contributors:
 *     Amir Kadić - initial API and implementation
 ******************************************************************************/
package tennis.domain.entity;

import tennis.domain.WinStatus;

public class PlayElement {

	private Score score;
	private WinStatus winStatus;
	private PlayElement onWin;
	private PlayElement onLoss;

	
	public PlayElement() {
		// for XStream compatibility only
	}	
	
	public PlayElement(Score score, WinStatus winStatus) {
		this.score = score;
		this.winStatus = winStatus;
	}

	public Score getScore() {
		return score;
	}

	public WinStatus getWinStatus() {
		return winStatus;
	}
	public PlayElement getOnWin() {
		return onWin;
	}

	public void setOnWin(PlayElement onWin) {
		this.onWin = onWin;
	}

	public PlayElement getOnLoss() {
		return onLoss;
	}

	public void setOnLoss(PlayElement onLoss) {
		this.onLoss = onLoss;
	}

}
