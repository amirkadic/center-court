/*******************************************************************************
 * Copyright (c) 2012 Amir Kadić.
 * 
 * This file is part of Center Court.
 * 
 * Center Court is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Center Court is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Center Court.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Contributors:
 *     Amir Kadić - initial API and implementation
 ******************************************************************************/
package tennis.app.gui.dto;

import android.os.Parcel;
import android.os.Parcelable;

public class ParcelableMatchReference implements Parcelable {

	public static final Parcelable.Creator<ParcelableMatchReference> CREATOR = new Parcelable.Creator<ParcelableMatchReference>() {

		public ParcelableMatchReference createFromParcel(Parcel parcel) {
			return new ParcelableMatchReference(parcel);
		}

		public ParcelableMatchReference[] newArray(int size) {
			return new ParcelableMatchReference[size];
		}
	};

	private long id;
	private String label;

	public ParcelableMatchReference(long id, String label) {
		this.id = id;
		this.label = label;
	}
	
	private ParcelableMatchReference(Parcel parcel) {
		this.id = parcel.readLong();
		this.label = parcel.readString();
	}

	public int describeContents() {
		return 0;
	}

	public void writeToParcel(Parcel parcel, int flags) {
		parcel.writeLong(this.id);
		parcel.writeString(this.label);
	}
	
	

	public long getId() {
		return id;
	}

	@Override
	public String toString() {
		return this.label;
	}

	
}
