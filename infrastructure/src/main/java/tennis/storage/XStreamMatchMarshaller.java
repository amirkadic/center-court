/*******************************************************************************
 * Copyright (c) 2012 Amir Kadić.
 * 
 * This file is part of Center Court.
 * 
 * Center Court is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Center Court is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Center Court.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Contributors:
 *     Amir Kadić - initial API and implementation
 ******************************************************************************/
package tennis.storage;

import tennis.domain.entity.Match;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.converters.enums.EnumConverter;
import com.thoughtworks.xstream.io.xml.JDomDriver;

public class XStreamMatchMarshaller implements MatchMarshaller {

	private final XStream xStream;

	public XStreamMatchMarshaller() {
		// xStream = new XStream(new DomDriver());
		xStream = new XStream(new JDomDriver());
		xStream.registerConverter(new EnumConverter());
		xStream.omitField(Match.class, "xmlns");
	}

	public String marshal(Match match) {
		synchronized (this.xStream) {
			return this.xStream.toXML(match);
		}
	}

	public Match unmarshal(String matchAsString) {
		return (Match) this.xStream.fromXML(matchAsString);
	}

}
