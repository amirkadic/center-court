/*******************************************************************************
 * Copyright (c) 2012 Amir Kadić.
 * 
 * This file is part of Center Court.
 * 
 * Center Court is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Center Court is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Center Court.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Contributors:
 *     Amir Kadić - initial API and implementation
 ******************************************************************************/
package tennis.domain.memento;

import java.util.Date;
import java.util.List;

import tennis.domain.WinStatus;
import tennis.domain.entity.CountableScore;
import tennis.domain.entity.Game;
import tennis.domain.entity.Point;
import tennis.domain.entity.Tiebreak;

public class TiebreakMemento extends GameMemento {

	private WinStatus status;
	private List<Point> points;
	private Date time;
	private boolean serve;
	private CountableScore score;

	public TiebreakMemento(WinStatus status, List<Point> points, Date time,
			boolean serve, CountableScore score) {
		super();
		this.status = status;
		this.points = points;
		this.time = time;
		this.serve = serve;
		this.score = score;
	}

	public Game restore() {
		return Tiebreak.restore(this);
	}

	public WinStatus getStatus() {
		return status;
	}

	public List<Point> getPoints() {
		return points;
	}

	public Date getTime() {
		return time;
	}

	public boolean isServe() {
		return serve;
	}

	public CountableScore getScore() {
		return score;
	}

}
