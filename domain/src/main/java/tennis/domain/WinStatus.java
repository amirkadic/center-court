/*******************************************************************************
 * Copyright (c) 2012 Amir Kadić.
 * 
 * This file is part of Center Court.
 * 
 * Center Court is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Center Court is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Center Court.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Contributors:
 *     Amir Kadić - initial API and implementation
 ******************************************************************************/
package tennis.domain;

public enum WinStatus {

	WON("won"), LOST("lost"), PENDING("pending");
	private String value;

	WinStatus(String value) {
		this.value = value;
	}

	public static WinStatus fromValue(String value) {
		for (WinStatus status : WinStatus.values()) {
			if (status.value.equals(value)) {
				return status;
			}
		}
		return null;
	}

	@Override
	public String toString() {
		return value;
	}

}
