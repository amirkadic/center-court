/*******************************************************************************
 * Copyright (c) 2012 Amir Kadić.
 * 
 * This file is part of Center Court.
 * 
 * Center Court is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Center Court is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Center Court.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Contributors:
 *     Amir Kadić - initial API and implementation
 ******************************************************************************/
package tennis.domain.memento;

import java.util.Date;
import java.util.List;

import tennis.domain.GameFactory;
import tennis.domain.WinStatus;
import tennis.domain.entity.CountableScore;

public class SetMemento {

	private WinStatus status;
	private CountableScore score;
	private List<GameMemento> gameMementos;
	private Date time;
	private GameFactory gameFactory;

	public SetMemento(WinStatus status, CountableScore score,
			List<GameMemento> gameMementos, Date time, GameFactory gameFactory) {
		this.status = status;
		this.score = score;
		this.gameMementos = gameMementos;
		this.time = time;
		this.gameFactory = gameFactory;

	}

	public WinStatus getStatus() {
		return status;
	}

	public CountableScore getScore() {
		return score;
	}

	public List<GameMemento> getGameMementos() {
		return gameMementos;
	}

	public Date getTime() {
		return time;
	}

	public GameFactory getGameFactory() {
		return gameFactory;
	}

}
