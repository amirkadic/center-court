/*******************************************************************************
 * Copyright (c) 2012 Amir Kadić.
 * 
 * This file is part of Center Court.
 * 
 * Center Court is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Center Court is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Center Court.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Contributors:
 *     Amir Kadić - initial API and implementation
 ******************************************************************************/
package tennis.domain.entity;

import java.util.Date;

import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import tennis.domain.GameFactory;
import tennis.domain.IllegalMoveException;
import tennis.domain.WinStatus;
import tennis.domain.entity.CountableScore;
import tennis.domain.entity.Game;
import tennis.domain.entity.Point;
import tennis.domain.memento.GameMemento;

public class GameTest {

	private static final GameFactory gameFactory = new GameFactory();

	@Before
	public void setup() {
	}
	
	@Test
	public void testAdd() throws IllegalMoveException {
		Game game = GameTest.gameFactory.startNewGame();
		Assert.assertEquals(WinStatus.PENDING, game.getStatus());
		Point point = new Point();
		point.setStatus(WinStatus.WON);
		point.setDate(new Date());
		game.add(point);
		Assert.assertEquals(new CountableScore(15, 0), game.getScore());
		Assert.assertEquals(new CountableScore(15, 0), point.getScore());
	}

	@Test
	public void testMemento() throws IllegalMoveException {
		Game game = GameTest.gameFactory.startNewGame();
		Assert.assertEquals(WinStatus.PENDING, game.getStatus());
		// 15:0
		this.winPoint(game);
		Assert.assertEquals(new CountableScore(15, 0), game.getScore());
		GameMemento memento = game.saveToMemento();
		// 30:0
		this.winPoint(game);
		Assert.assertEquals(new CountableScore(30, 0), game.getScore());
		// restore to 15:0
		game = memento.restore();
		Assert.assertEquals(new CountableScore(15, 0), game.getScore());
		// 30:0 again
		this.winPoint(game);
		// 40:0
		this.winPoint(game);
		// game first player
		this.winPoint(game);		
		Assert.assertEquals(WinStatus.WON, game.getStatus());
		// restore to 15:0
		game = memento.restore();
		Assert.assertEquals(new CountableScore(15, 0), game.getScore());
		Assert.assertEquals(WinStatus.PENDING, game.getStatus());

	}
	
	@Test
	public void testLLWBug() throws IllegalMoveException {
		Game game = GameTest.gameFactory.startNewGame();
		Assert.assertEquals(WinStatus.PENDING, game.getStatus());
		this.losePoint(game);
		Assert.assertEquals(new CountableScore(0, 15), game.getScore());
		this.losePoint(game);
		Assert.assertEquals(new CountableScore(0, 30), game.getScore());
		this.winPoint(game);
		Assert.assertEquals(new CountableScore(15, 30), game.getScore());
	}

	
	private void winPoint(Game game) throws IllegalMoveException {
		Point point = new Point();
		point.setStatus(WinStatus.WON);
		game.add(point);
		
	}

	private void losePoint(Game game) throws IllegalMoveException {
		Point point = new Point();
		point.setStatus(WinStatus.LOST);
		game.add(point);
		
	}

}
