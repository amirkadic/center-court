/*******************************************************************************
 * Copyright (c) 2012 Amir Kadić.
 * 
 * This file is part of Center Court.
 * 
 * Center Court is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Center Court is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Center Court.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Contributors:
 *     Amir Kadić - initial API and implementation
 ******************************************************************************/
package tennis.domain.value;

import java.util.ArrayList;
import java.util.Arrays;

import org.junit.Test;

import tennis.domain.IllegalScoreException;
import tennis.domain.entity.CountableScore;
import tennis.domain.value.BestOf3ScoreInSets;

public class BestOf3ScoreInSetsTest {

	@Test(expected = IllegalScoreException.class)
	public void winning3SetsIsImpossible() throws IllegalScoreException {
		BestOf3ScoreInSets score = new BestOf3ScoreInSets(Arrays.asList(
				new CountableScore(6, 0), new CountableScore(6, 0),
				new CountableScore(6, 0)));
		score.validate();
	}

	@Test(expected = IllegalScoreException.class)
	public void sevenToOtherThan6IsIllegal() throws IllegalScoreException {
		BestOf3ScoreInSets score = new BestOf3ScoreInSets(
				Arrays.asList(new CountableScore(7, 0)));
		score.validate();
	}

	@Test(expected = IllegalScoreException.class)
	public void noSets() throws IllegalScoreException {
		BestOf3ScoreInSets score = new BestOf3ScoreInSets(
				new ArrayList<CountableScore>());
		score.validate();
	}

	@Test(expected = IllegalScoreException.class)
	public void setsUndefined() throws IllegalScoreException {
		BestOf3ScoreInSets score = new BestOf3ScoreInSets(null);
		score.validate();
	}

	@Test(expected = IllegalScoreException.class)
	public void twoUnfinishedSets() throws IllegalScoreException {
		BestOf3ScoreInSets score = new BestOf3ScoreInSets(Arrays.asList(
				new CountableScore(5, 0), new CountableScore(5, 0)));
		score.validate();
	}

}
