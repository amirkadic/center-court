/*******************************************************************************
 * Copyright (c) 2012 Amir Kadić.
 * 
 * This file is part of Center Court.
 * 
 * Center Court is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Center Court is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Center Court.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Contributors:
 *     Amir Kadić - initial API and implementation
 ******************************************************************************/
package tennis.app.gui.activity;

import java.util.List;

import tennis.app.gui.dto.ParcelableMatchReference;
import tennis.domain.entity.Match;
import tennis.storage.MatchStore;
import tennis.storage.StorageException;
import tennis.web.service.MatchService;
import tennis.web.service.RestfulMatchService;
import android.graphics.Color;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.TextView;

public class UploadTask extends AsyncTask<ParcelableMatchReference, Void, Void>{

	private MatchService webService = RestfulMatchService.getService();
	private MatchStore matchStore;
	private TextView matchView;
	
	
	

	public UploadTask(MatchStore matchStore, TextView matchView) {
		super();
		this.matchStore = matchStore;
		this.matchView = matchView;
	}


	@Override
	protected Void doInBackground(ParcelableMatchReference... argList) {
		ParcelableMatchReference matchReference = argList[0];
		List<Match> matches = null;
		try {
			matches = matchStore.allMatches();

		} catch (StorageException e) {
			Log.e("MatchList", e.getMessage());
			return null;
		}
		for (Match match : matches) {
			if (match.getId() == matchReference.getId()) {
				this.webService.upload(match);
				Log.d("MatchList", "Deleted match " + match.getId());
				return null;
			}
		}

		return null;
	}


	@Override
	protected void onPostExecute(Void result) {
		this.matchView.setBackgroundColor(Color.GREEN);
	}

}
