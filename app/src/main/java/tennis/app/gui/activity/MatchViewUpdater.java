/*******************************************************************************
 * Copyright (c) 2012 Amir Kadić.
 * 
 * This file is part of Center Court.
 * 
 * Center Court is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Center Court is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Center Court.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Contributors:
 *     Amir Kadić - initial API and implementation
 ******************************************************************************/
package tennis.app.gui.activity;

import tennis.domain.entity.Match;

/**
 * Updates the UI based on the state of a match.
 * 
 * @author amir
 * 
 */
public interface MatchViewUpdater {

	/**
	 * Make sure to only call this from the UI thread.
	 * 
	 * @param match
	 *            The match the state of which is to be reflected in the UI.
	 */
	void update(Match match);
	/**
	 * Remove match info from the UI, e.g. if no match is being tracked.
	 */
	void clear();
}
