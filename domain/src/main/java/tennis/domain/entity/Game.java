/*******************************************************************************
 * Copyright (c) 2012 Amir Kadić.
 * 
 * This file is part of Center Court.
 * 
 * Center Court is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Center Court is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Center Court.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Contributors:
 *     Amir Kadić - initial API and implementation
 ******************************************************************************/
package tennis.domain.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import tennis.domain.IllegalMoveException;
import tennis.domain.WinStatus;
import tennis.domain.memento.GameMemento;

import com.thoughtworks.xstream.annotations.XStreamImplicit;

public abstract class Game {

	protected WinStatus status = WinStatus.PENDING;
	@XStreamImplicit(itemFieldName = "point")
	protected List<Point> points = new ArrayList<Point>();
	protected Date time;
	protected boolean serve;
	protected long id;

	public abstract void start();

	public abstract void add(Point point) throws IllegalMoveException;

	public WinStatus getStatus() {
		return status;
	}

	public abstract Score getScore();

	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	public boolean isServe() {
		return serve;
	}

	public void setServe(boolean serve) {
		this.serve = serve;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public abstract GameMemento saveToMemento();

	public Point getCurrentPoint() {
		if (this.points.isEmpty()) {
			return null;
		} else {
			return this.points.get(this.points.size() - 1);
		}
	}
	
	public List<Point> getPoints() {
		return this.points;
	}
}
