/*******************************************************************************
 * Copyright (c) 2012 Amir Kadić.
 * 
 * This file is part of Center Court.
 * 
 * Center Court is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Center Court is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Center Court.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Contributors:
 *     Amir Kadić - initial API and implementation
 ******************************************************************************/
package tennis.app.gui.activity;

import tennis.domain.entity.Match;
import tennis.domain.service.TrackingService;
import tennis.storage.MatchStore;
import tennis.storage.StorageException;
import android.os.AsyncTask;
import android.util.Log;

class ResumeMatchTask extends AsyncTask<Long, Void, Match> {

	private TrackingService service;
	private MatchStore matchStore;
	private MatchViewUpdater matchViewUpdater;

	public ResumeMatchTask(TrackingService service, MatchStore matchStore,
			MatchViewUpdater matchViewUpdater) {
		super();
		this.service = service;
		this.matchStore = matchStore;
		this.matchViewUpdater = matchViewUpdater;
	}

	@Override
	protected Match doInBackground(Long... argList) {
		Long input = argList[0];
		Match resumee = null;
		try {
			resumee = matchStore.getByDate(input);
			if (resumee != null) {
				Log.d("ResumeMatchProducer", "Found resumee for date "
						+ input + ".");
				if (this.service.isRunning()) {
					this.matchStore.put(this.service.getMatch());
				}
				service.resume(resumee);
				return resumee;
			} else {
				Log.w("ResumeMatchProducer", "Weird. No match dating from "
						+ input + " was found.");
			}
		} catch (StorageException e) {
			Log.d("ResumeMatchTask",
					"Error saving current match. " + e.getMessage());
		}
		return null;
	}

	@Override
	protected void onPostExecute(Match result) {
		if (result != null) {
			Log.d("ResumeMatchCommand", "Resuming match from date "
					+ result.getTime() + "(" + result.getPlayer().getName()
					+ " vs " + result.getOpponent().getName() + ").");
			this.matchViewUpdater.update(result);
		} else {
			Log.w("ResumeMatchCommand", "No match to resume.");
		}
	}

}
