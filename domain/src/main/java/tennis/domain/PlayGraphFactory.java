/*******************************************************************************
 * Copyright (c) 2012 Amir Kadić.
 * 
 * This file is part of Center Court.
 * 
 * Center Court is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Center Court is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Center Court.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Contributors:
 *     Amir Kadić - initial API and implementation
 ******************************************************************************/
package tennis.domain;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import tennis.domain.entity.Advantage;
import tennis.domain.entity.CountableScore;
import tennis.domain.entity.Deuce;
import tennis.domain.entity.Disadvantage;
import tennis.domain.entity.EndScore;
import tennis.domain.entity.PlayElement;
import tennis.domain.entity.PlayGraph;
import tennis.domain.entity.Score;

public class PlayGraphFactory {
	public static final Collection<Score> allPossibleScores;
	// all game scores
	private static final CountableScore initialGameScore = new CountableScore(
			(short) 0, (short) 0);
	private static final CountableScore fifteenLoveScore = new CountableScore(
			(short) 15, (short) 0);
	private static final CountableScore thirtyLoveScore = new CountableScore(
			(short) 30, (short) 0);
	private static final CountableScore fortyLoveScore = new CountableScore(
			(short) 40, (short) 0);
	private static final CountableScore loveFifteenScore = new CountableScore(
			(short) 0, (short) 15);
	private static final CountableScore loveThirtyScore = new CountableScore(
			(short) 0, (short) 30);
	private static final CountableScore loveFortyScore = new CountableScore(
			(short) 0, (short) 40);
	private static final CountableScore fifteenAllScore = new CountableScore(
			(short) 15, (short) 15);
	private static final CountableScore thirtyFifteenScore = new CountableScore(
			(short) 30, (short) 15);
	private static final CountableScore fortyFifteenScore = new CountableScore(
			(short) 40, (short) 15);
	private static final CountableScore fifteenThirtyScore = new CountableScore(
			(short) 15, (short) 30);
	private static final CountableScore fifteenFortyScore = new CountableScore(
			(short) 15, (short) 40);
	private static final CountableScore thirtyAllScore = new CountableScore(
			(short) 30, (short) 30);
	private static final CountableScore fortyThirtyScore = new CountableScore(
			(short) 40, (short) 30);
	private static final CountableScore thirtyFortyScore = new CountableScore(
			(short) 30, (short) 40);
	private static final Score deuceScore = new Deuce();
	private static final Score advantageScore = new Advantage();
	private static final Score disadvantageScore = new Disadvantage();
	private static final Score gameWonScore = new EndScore();
	private static final Score gameLostScore = new EndScore();
	private static final PlayElement initialElement;
	private static final PlayElement fifteenLoveElement;
	private static final PlayElement thirtyLoveElement;
	private static final PlayElement fortyLoveElement;
	private static final PlayElement loveFifteenElement;
	private static final PlayElement loveThirtyElement;
	private static final PlayElement loveFortyElement;
	private static final PlayElement fifteenAllElement;
	private static final PlayElement thirtyFifteenElement;
	private static final PlayElement fortyFifteenElement;
	private static final PlayElement fifteenThirtyElement;
	private static final PlayElement fifteenFortyElement;
	private static final PlayElement thirtyAllElement;
	private static final PlayElement fortyThirtyElement;
	private static final PlayElement thirtyFortyElement;
	private static final PlayElement deuceElement;
	private static final PlayElement advantageElement;
	private static final PlayElement disadvantageElement;
	private static final PlayElement gameWonElement;
	private static final PlayElement gameLostElement;
	private static final List<PlayElement> elements;

	static {
		initialElement = new PlayElement(PlayGraphFactory.initialGameScore,
				WinStatus.PENDING);
		fifteenLoveElement = new PlayElement(PlayGraphFactory.fifteenLoveScore,
				WinStatus.PENDING);
		thirtyLoveElement = new PlayElement(PlayGraphFactory.thirtyLoveScore,
				WinStatus.PENDING);
		fortyLoveElement = new PlayElement(PlayGraphFactory.fortyLoveScore,
				WinStatus.PENDING);
		loveFifteenElement = new PlayElement(PlayGraphFactory.loveFifteenScore,
				WinStatus.PENDING);
		loveThirtyElement = new PlayElement(PlayGraphFactory.loveThirtyScore,
				WinStatus.PENDING);
		loveFortyElement = new PlayElement(PlayGraphFactory.loveFortyScore,
				WinStatus.PENDING);
		fifteenAllElement = new PlayElement(PlayGraphFactory.fifteenAllScore,
				WinStatus.PENDING);
		thirtyFifteenElement = new PlayElement(
				PlayGraphFactory.thirtyFifteenScore, WinStatus.PENDING);
		fortyFifteenElement = new PlayElement(
				PlayGraphFactory.fortyFifteenScore, WinStatus.PENDING);
		fifteenThirtyElement = new PlayElement(
				PlayGraphFactory.fifteenThirtyScore, WinStatus.PENDING);
		fifteenFortyElement = new PlayElement(
				PlayGraphFactory.fifteenFortyScore, WinStatus.PENDING);
		thirtyAllElement = new PlayElement(PlayGraphFactory.thirtyAllScore,
				WinStatus.PENDING);
		fortyThirtyElement = new PlayElement(PlayGraphFactory.fortyThirtyScore,
				WinStatus.PENDING);
		thirtyFortyElement = new PlayElement(PlayGraphFactory.thirtyFortyScore,
				WinStatus.PENDING);
		deuceElement = new PlayElement(PlayGraphFactory.deuceScore,
				WinStatus.PENDING);
		advantageElement = new PlayElement(PlayGraphFactory.advantageScore,
				WinStatus.PENDING);
		disadvantageElement = new PlayElement(
				PlayGraphFactory.disadvantageScore, WinStatus.PENDING);
		gameWonElement = new PlayElement(PlayGraphFactory.gameWonScore,
				WinStatus.WON);
		gameLostElement = new PlayElement(PlayGraphFactory.gameLostScore,
				WinStatus.LOST);

		initialElement.setOnWin(fifteenLoveElement);
		initialElement.setOnLoss(loveFifteenElement);
		fifteenLoveElement.setOnWin(thirtyLoveElement);
		fifteenLoveElement.setOnLoss(fifteenAllElement);
		thirtyLoveElement.setOnWin(fortyLoveElement);
		thirtyLoveElement.setOnLoss(thirtyFifteenElement);
		fortyLoveElement.setOnWin(gameWonElement);
		fortyLoveElement.setOnLoss(fortyFifteenElement);

		loveFifteenElement.setOnWin(fifteenAllElement);
		loveFifteenElement.setOnLoss(loveThirtyElement);
		loveThirtyElement.setOnWin(fifteenThirtyElement);
		loveThirtyElement.setOnLoss(loveFortyElement);
		loveFortyElement.setOnWin(fifteenFortyElement);
		loveFortyElement.setOnLoss(gameLostElement);

		fifteenAllElement.setOnWin(thirtyFifteenElement);
		fifteenAllElement.setOnLoss(fifteenThirtyElement);
		thirtyFifteenElement.setOnWin(fortyFifteenElement);
		thirtyFifteenElement.setOnLoss(thirtyAllElement);
		fortyFifteenElement.setOnWin(gameWonElement);
		fortyFifteenElement.setOnLoss(fortyThirtyElement);

		fifteenThirtyElement.setOnWin(thirtyAllElement);
		fifteenThirtyElement.setOnLoss(fifteenFortyElement);
		fifteenFortyElement.setOnWin(thirtyFortyElement);
		fifteenFortyElement.setOnLoss(gameLostElement);

		thirtyAllElement.setOnWin(fortyThirtyElement);
		thirtyAllElement.setOnLoss(thirtyFortyElement);
		fortyThirtyElement.setOnWin(gameWonElement);
		fortyThirtyElement.setOnLoss(deuceElement);

		thirtyFortyElement.setOnWin(deuceElement);
		thirtyFortyElement.setOnLoss(gameLostElement);

		deuceElement.setOnWin(advantageElement);
		advantageElement.setOnWin(gameWonElement);

		deuceElement.setOnLoss(disadvantageElement);
		disadvantageElement.setOnLoss(gameLostElement);

		disadvantageElement.setOnWin(deuceElement);
		advantageElement.setOnLoss(deuceElement);

		elements = Arrays.asList(initialElement, fifteenLoveElement,
				thirtyLoveElement, fortyLoveElement, loveFifteenElement,
				loveThirtyElement, loveFortyElement, fifteenAllElement,
				thirtyFifteenElement, fortyFifteenElement,
				fifteenThirtyElement, fifteenFortyElement, thirtyAllElement,
				fortyThirtyElement, thirtyFortyElement, deuceElement,
				advantageElement, disadvantageElement, gameWonElement,
				gameLostElement);
		allPossibleScores = Collections.unmodifiableCollection(Arrays.asList(initialGameScore, fifteenLoveScore,
				thirtyLoveScore, fortyLoveScore, loveFifteenScore,
				loveThirtyScore, loveFortyScore, fifteenAllScore,
				thirtyFifteenScore, fortyFifteenScore, fifteenThirtyScore,
				fifteenFortyScore, thirtyAllScore, fortyThirtyScore,
				thirtyFortyScore, deuceScore, advantageScore,
				disadvantageScore, gameWonScore, gameLostScore));
	}

	public PlayGraph newGameGraph() {

		return new PlayGraph(initialElement);
	}

	public PlayGraph fromScore(Score score) throws IllegalMoveException {
		for (PlayElement element : PlayGraphFactory.elements) {
			if (element.getScore().equals(score)) {
				return new PlayGraph(element);
			}
		}
		throw new IllegalMoveException("Invalid game score: " + score + ".");
	}
}
