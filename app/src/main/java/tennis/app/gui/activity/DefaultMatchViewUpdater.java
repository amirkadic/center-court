/*******************************************************************************
 * Copyright (c) 2012 Amir Kadić.
 * 
 * This file is part of Center Court.
 * 
 * Center Court is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Center Court is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Center Court.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Contributors:
 *     Amir Kadić - initial API and implementation
 ******************************************************************************/
package tennis.app.gui.activity;

import java.util.Iterator;

import tennis.domain.WinStatus;
import tennis.domain.entity.CountableScore;
import tennis.domain.entity.Match;
import tennis.domain.entity.Player;
import tennis.domain.value.MatchScore;
import android.util.Log;
import android.widget.Button;
import android.widget.TextView;

class DefaultMatchViewUpdater implements MatchViewUpdater {

	private Button winButton;
	private Button loseButton;
	private TextView playerNamesTextView;
	private TextView setScoresTextView;
	private TextView gameScoreTextView;
	private Button undoButton;
	private Button jumpButton;

	public DefaultMatchViewUpdater(Button winButton, Button loseButton,
			TextView playerNamesTextView, TextView setScoresTextView,
			TextView gameScoreTextView, Button undoButton, Button jumpButton) {
		this.winButton = winButton;
		this.loseButton = loseButton;
		this.playerNamesTextView = playerNamesTextView;
		this.setScoresTextView = setScoresTextView;
		this.gameScoreTextView = gameScoreTextView;
		this.undoButton = undoButton;
		this.jumpButton = jumpButton;
	}

	/***
	 * {@inheritDoc}
	 */
	public void update(Match match) {
		this.ableControls(match.getStatus() == WinStatus.PENDING);

		Player player = match.getPlayer();
		Player opponent = match.getOpponent();

		if (player != null && opponent != null) {
			String playerName = player.getName();
			String opponentName = opponent.getName();
			this.playerNamesTextView
					.setText(playerName + " vs " + opponentName);
		} else {
			Log.w("DefaultMatchViewUpdater", "match has no player");
		}

		MatchScore score = match.currentScore();
		String setScoresText = "";
		Iterator<CountableScore> setScores = score.getScoreInSets().iterator();
		do {
			setScoresText += " " + setScores.next();
		} while (setScores.hasNext());
		setScoresText = setScoresText.trim();
		this.setScoresTextView.setText(setScoresText);
		String lcGameScore = score.getGameScore().toString();
		String gameScore = lcGameScore.substring(0, 1).concat(
				lcGameScore.substring(1));
		this.gameScoreTextView.setText(gameScore);
		this.undoButton.setEnabled(true);
		this.jumpButton.setEnabled(true);
	}

	/***
	 * {@inheritDoc}
	 */
	public void clear() {
		this.ableControls(false);
		this.playerNamesTextView.setText("");
		this.setScoresTextView.setText("");
		this.gameScoreTextView.setText("");
		this.undoButton.setEnabled(false);
		this.jumpButton.setEnabled(false);
	}

	private void ableControls(boolean enabled) {
		this.winButton.setEnabled(enabled);
		this.loseButton.setEnabled(enabled);
	}

}
