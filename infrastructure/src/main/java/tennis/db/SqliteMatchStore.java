/*******************************************************************************
 * Copyright (c) 2012 Amir Kadić.
 * 
 * This file is part of Center Court.
 * 
 * Center Court is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Center Court is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Center Court.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Contributors:
 *     Amir Kadić - initial API and implementation
 ******************************************************************************/
package tennis.db;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.util.Assert;

import tennis.domain.GameFactory;
import tennis.domain.Handedness;
import tennis.domain.IllegalMoveException;
import tennis.domain.PlayGraphFactory;
import tennis.domain.SetFactory;
import tennis.domain.Surface;
import tennis.domain.WinStatus;
import tennis.domain.entity.Advantage;
import tennis.domain.entity.CountableScore;
import tennis.domain.entity.Deuce;
import tennis.domain.entity.Disadvantage;
import tennis.domain.entity.EndScore;
import tennis.domain.entity.Game;
import tennis.domain.entity.Match;
import tennis.domain.entity.Player;
import tennis.domain.entity.Point;
import tennis.domain.entity.RegularGame;
import tennis.domain.entity.Score;
import tennis.domain.entity.Set;
import tennis.domain.entity.Tiebreak;
import tennis.domain.entity.TrackingStatus;
import tennis.storage.MatchStore;
import tennis.storage.PlayerStore;
import tennis.storage.StorageException;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class SqliteMatchStore implements MatchStore {

	// singleton instance of this class
	private static SqliteMatchStore singleton;

	// injectable dependencies
	private SQLiteDatabase readableDb;
	private SQLiteDatabase writableDb;
	private PlayerStore playerStore;

	private Map<String, Score> scoreSingletons = new HashMap<String, Score>();
	private List<String> namedScores = Arrays.asList("deuce", "advantage",
			"disadvantage", "end");
	private PlayGraphFactory playGraphFactory = new PlayGraphFactory();

	public static MatchStore getSingleton(Context context) {
		if (singleton == null) {
			singleton = new SqliteMatchStore();
			OpenHelper openHelper = OpenHelper.getStore(context);
			singleton.setReadableDb(openHelper.getReadableDatabase());
			singleton.setWritableDb(openHelper.getWritableDatabase());
			singleton.setPlayerStore(SqlitePlayerStore.getSingelton(context));
		}
		return singleton;
	}

	public SqliteMatchStore() {
		this.scoreSingletons.put("deuce", new Deuce());
		this.scoreSingletons.put("advantage", new Advantage());
		this.scoreSingletons.put("disadvantage", new Disadvantage());
		this.scoreSingletons.put("end", new EndScore());
	}

	// MatchStore interface implementation
	public void put(Match match) throws StorageException {

		ContentValues values = new ContentValues(9);
		// FIXME actually updating might be a better idea
		if (match.getId() != 0) {
			this.writableDb.delete(OpenHelper.MATCH_TABLE,
					OpenHelper.MATCH_COLUMN_ID + "=" + match.getId(), null);
			values.put(OpenHelper.MATCH_COLUMN_ID, match.getId());
		}

		Player player = match.getPlayer();
		Player opponent = match.getOpponent();
		if (player.getId() == 0) {
			this.playerStore.put(player);
		}
		if (opponent.getId() == 0) {
			this.playerStore.put(opponent);
		}
		values.put(OpenHelper.MATCH_COLUMN_PLAYERID, player.getId());
		values.put(OpenHelper.MATCH_COLUMN_OPPONENTID, opponent.getId());

		values.put(OpenHelper.MATCH_COLUMN_SCORE, match.getCurrentScore()
				.toString());
		values.put(OpenHelper.MATCH_COLUMN_SERVEFIRST, match.isServeFirst() ? 1
				: 0);
		values.put(OpenHelper.MATCH_COLUMN_TIME, match.getTime());
		values.put(OpenHelper.MATCH_COLUMN_TRACKINGSTATUS, match
				.getTrackingStatus().toString());
		values.put(OpenHelper.MATCH_COLUMN_WINSTATUS, match.getStatus()
				.toString());
		if (match.getSurface() != null) {
			values.put(OpenHelper.MATCH_COLUMN_SURFACE, match.getSurface()
					.toString());
		}
		values.put(OpenHelper.MATCH_COLUMN_NOTE, match.getNote());
		long id = this.writableDb.insert(OpenHelper.MATCH_TABLE, null, values);
		Assert.isTrue(id != 0);
		match.setId(id);
		for (Set set : match.getSets()) {
			this.putSet(set, id);
		}

	}

	public List<Match> allMatches() throws StorageException {
		List<Match> allMatches = new ArrayList<Match>();
		Cursor matchCursor = this.readableDb.query(OpenHelper.MATCH_TABLE,
				null, null, null, null, null, null);
		while (matchCursor.moveToNext()) {
			allMatches.add(this.mapCursorRowToMatch(matchCursor));
		}
		matchCursor.close();
		return allMatches;
	}

	public void remove(Match match) {
		if (1 == this.writableDb.delete(OpenHelper.MATCH_TABLE,
				OpenHelper.MATCH_COLUMN_ID + "=" + match.getId(), null)) {
			match.setId(0);
		}
	}

	public boolean remove(long matchId) {
		return 1 == this.writableDb.delete(OpenHelper.MATCH_TABLE,
				OpenHelper.MATCH_COLUMN_ID + "=" + matchId, null);

	}

	public Match getLatest() throws StorageException {
		Cursor matchCursor = this.readableDb.query(OpenHelper.MATCH_TABLE,
				null, null, null, null, null, OpenHelper.MATCH_COLUMN_TIME
						+ " DESC", "1");
		Match match = null;
		if (matchCursor.moveToNext()) {
			match = mapCursorRowToMatch(matchCursor);
		}
		matchCursor.close();
		return match;
	}

	public Match getByDate(long date) throws StorageException {
		Cursor matchCursor = this.readableDb.query(OpenHelper.MATCH_TABLE,
				null, OpenHelper.MATCH_COLUMN_TIME + "=" + date, null, null,
				null, null);
		Match match = null;
		if (matchCursor.getCount() > 1) {
			throw new StorageException("Found more than 1 (i.e. "
					+ matchCursor.getCount() + ") matches with date " + date
					+ ".");
		}
		if (matchCursor.moveToNext()) {
			match = mapCursorRowToMatch(matchCursor);
		}
		matchCursor.close();
		return match;
	}

	public Match getById(long id) throws StorageException {
		Cursor matchCursor = this.readableDb.query(OpenHelper.MATCH_TABLE,
				null, OpenHelper.MATCH_COLUMN_ID + "=" + id, null, null, null,
				null);
		Match match = null;
		if (matchCursor.getCount() > 1) {
			throw new StorageException("Found more than 1 (i.e. "
					+ matchCursor.getCount() + ") matches with id " + id + ".");
		}
		if (matchCursor.moveToNext()) {
			match = mapCursorRowToMatch(matchCursor);
		}
		matchCursor.close();
		return match;
	}

	// setters

	public void setReadableDb(SQLiteDatabase readableDb) {
		this.readableDb = readableDb;
	}

	public void setWritableDb(SQLiteDatabase writableDb) {
		this.writableDb = writableDb;
	}

	public void setPlayerStore(PlayerStore playerStore) {
		this.playerStore = playerStore;
	}

	// private methods

	private Match mapCursorRowToMatch(Cursor matchCursor)
			throws StorageException {
		long matchId = matchCursor.getLong(0);
		long opponentId = matchCursor.getLong(1);
		// TODO get rid of these hard-coded values for players
		Player opponent = null;
		if (opponentId != 0) {
			opponent = this.playerStore.getPlayer(opponentId);
			opponent.setHandedness(Handedness.RIGHT);
			opponent.setAge(20);
			opponent.setRanking("good ranking");
			opponent.setSurface(Surface.CLAY);
		}
		long playerId = matchCursor.getLong(2);
		Player player = null;
		if (playerId != 0) {
			player = this.playerStore.getPlayer(playerId);
			player.setHandedness(Handedness.RIGHT);
			player.setAge(20);
			player.setRanking("very good ranking");
			player.setSurface(Surface.OTHER);
		}
		CountableScore currentScore = countableScoreFromString(matchCursor
				.getString(3));
		boolean serveFirst = matchCursor.getInt(4) == 1;
		long time = matchCursor.getLong(5);
		String dbTrackingStatus = matchCursor.getString(6);
		String dbWinStatus = matchCursor.getString(7);
		TrackingStatus trackingStatus = TrackingStatus
				.fromValue(dbTrackingStatus);
		WinStatus winStatus = WinStatus.fromValue(dbWinStatus);
		String dbSurface = matchCursor.getString(8);
		Surface surface = Surface.fromValue(dbSurface);
		String note = matchCursor.getString(9);

		GameFactory gameFactory = new GameFactory();
		List<Set> sets = this.setsOf(matchId, gameFactory);
		SetFactory setFactory = new SetFactory();
		setFactory.setGameFactory(gameFactory);
		return new Match(matchId, time, serveFirst, player, opponent,
				trackingStatus, winStatus, currentScore, setFactory, sets,
				surface, note);
	}

	private void putSet(Set set, long matchId) {
		if (set.getId() != 0) {
			this.writableDb.delete(OpenHelper.SET_TABLE,
					OpenHelper.SET_COLUMN_ID + "=" + set.getId(), null);
		}
		ContentValues values = new ContentValues(3);
		values.put(OpenHelper.SET_COLUMN_MATCH_ID, matchId);
		values.put(OpenHelper.SET_COLUMN_SCORE, set.getCurrentScore()
				.toString());
		Date time = set.getTime();
		values.put(OpenHelper.SET_COLUMN_TIME, time != null ? time.getTime()
				: null);
		values.put(OpenHelper.SET_COLUMN_WINSTATUS, set.getStatus().toString());
		long id = this.writableDb.insert(OpenHelper.SET_TABLE, null, values);
		Assert.isTrue(id != 0);
		set.setId(id);
		for (Game game : set.getGames()) {
			this.putGame(game, id);
		}
	}

	private void putGame(Game game, long setId) {
		if (game.getId() != 0) {
			this.writableDb.delete(OpenHelper.GAME_TABLE,
					OpenHelper.GAME_COLUMN_ID + "=" + game.getId(), null);
		}
		ContentValues values = new ContentValues(5);
		values.put(OpenHelper.GAME_COLUMN_SET_ID, setId);
		values.put(OpenHelper.GAME_COLUMN_SCORE, game.getScore().toString());
		values.put(OpenHelper.GAME_COLUMN_SERVE, game.isServe() ? 1 : 0);
		values.put(OpenHelper.GAME_COLUMN_TIME, game.getTime().getTime());
		values.put(OpenHelper.GAME_COLUMN_TYPE, game instanceof RegularGame ? 0
				: 1);
		values.put(OpenHelper.GAME_COLUMN_WINSTATUS, game.getStatus()
				.toString());
		long id = this.writableDb.insert(OpenHelper.GAME_TABLE, null, values);
		Assert.isTrue(id != 0);
		game.setId(id);
		for (Point point : game.getPoints()) {
			this.putPoint(point, id);
		}
	}

	private void putPoint(Point point, long gameId) {
		if (point.getId() != 0) {
			this.writableDb.delete(OpenHelper.POINT_TABLE,
					OpenHelper.POINT_COLUMN_ID + "=" + point.getId(), null);
		}
		ContentValues values = new ContentValues(4);
		values.put(OpenHelper.POINT_COLUMN_GAME_ID, gameId);
		values.put(OpenHelper.POINT_COLUMN_COMMENT, point.getComment());
		values.put(OpenHelper.POINT_COLUMN_SCORE, point.getScore().toString());
		values.put(OpenHelper.POINT_COLUMN_TIME, point.getDate().getTime());
		values.put(OpenHelper.POINT_COLUMN_WINSTATUS, point.getStatus()
				.toString());
		long id = this.writableDb.insert(OpenHelper.POINT_TABLE, null, values);
		Assert.isTrue(id != 0);
		point.setId(id);
	}

	private List<Set> setsOf(long matchId, GameFactory gameFactory)
			throws StorageException {
		List<Set> sets = new ArrayList<Set>();
		Cursor setCursor = this.readableDb.query(OpenHelper.SET_TABLE, null,
				OpenHelper.SET_COLUMN_MATCH_ID + "=" + matchId, null, null,
				null, null);
		while (setCursor.moveToNext()) {
			// id, matchid, score, time, winstatus
			long setId = setCursor.getLong(0);
			CountableScore score = countableScoreFromString(setCursor
					.getString(2));
			Date setTime = null;
			if (!setCursor.isNull(3)) {
				setTime = new Date(setCursor.getLong(3));
			}
			WinStatus setWinStatus = WinStatus
					.fromValue(setCursor.getString(4));
			List<Game> games = this.gamesOf(setId);
			Set set = new Set(setId, setTime, score, setWinStatus, games,
					gameFactory);
			sets.add(set);
		}
		setCursor.close();
		return sets;
	}

	private List<Game> gamesOf(long setId) throws StorageException {
		List<Game> games = new ArrayList<Game>();
		Cursor gameCursor = this.readableDb.query(OpenHelper.GAME_TABLE, null,
				OpenHelper.GAME_COLUMN_SET_ID + "=" + setId, null, null, null,
				null);
		while (gameCursor.moveToNext()) {
			// id, score, serve, time, type, winstatus
			long gameId = gameCursor.getLong(0);
			String scoreText = gameCursor.getString(2);
			boolean serve = gameCursor.getInt(3) == 1;
			Date time = null;
			if (!gameCursor.isNull(4)) {
				time = new Date(gameCursor.getLong(4));
			}
			WinStatus winStatus = WinStatus.fromValue(gameCursor.getString(6));
			boolean tiebreak = gameCursor.getInt(5) == 1;
			if (tiebreak) {
				games.add(new Tiebreak(gameId, winStatus, time, serve,
						countableScoreFromString(scoreText)));
			} else {
				try {
					List<Point> points = pointsOf(gameId);
					games.add(new RegularGame(gameId, winStatus, points, time,
							serve, scoreFromString(scoreText),
							this.playGraphFactory));
				} catch (IllegalMoveException e) {
					throw new StorageException("Illegal score stored in db.");
				}
			}

		}
		gameCursor.close();
		return games;
	}

	private List<Point> pointsOf(long gameId) {
		List<Point> points = new ArrayList<Point>();
		// id, comment, score, time, winstatus
		Cursor pointCursor = this.readableDb.query(OpenHelper.POINT_TABLE,
				null, OpenHelper.POINT_COLUMN_GAME_ID + "=" + gameId, null,
				null, null, null);
		while (pointCursor.moveToNext()) {
			Point point = new Point();
			point.setId(pointCursor.getLong(0));
			point.setComment(pointCursor.getString(2));
			point.setScore(scoreFromString(pointCursor.getString(3)));
			point.setDate(new Date(pointCursor.getLong(4)));
			point.setStatus(WinStatus.fromValue(pointCursor.getString(5)));
			points.add(point);
		}
		pointCursor.close();
		return points;
	}

	private CountableScore countableScoreFromString(String text) {
		String splitScore[] = text.split(":");
		int setsWon = Integer.parseInt(splitScore[0]);
		int setsLost = Integer.parseInt(splitScore[1]);
		return new CountableScore(setsWon, setsLost);
	}

	private Score scoreFromString(String text) {
		if (this.namedScores.contains(text)) {
			return this.scoreSingletons.get(text);
		} else {
			return countableScoreFromString(text);
		}
	}

}
