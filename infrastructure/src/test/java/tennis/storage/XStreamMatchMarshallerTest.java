/*******************************************************************************
 * Copyright (c) 2012 Amir Kadić.
 * 
 * This file is part of Center Court.
 * 
 * Center Court is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Center Court is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Center Court.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Contributors:
 *     Amir Kadić - initial API and implementation
 ******************************************************************************/
package tennis.storage;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;

import junit.framework.Assert;

import org.junit.Test;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;

import tennis.domain.IllegalMoveException;
import tennis.domain.PointFactory;
import tennis.domain.SetFactory;
import tennis.domain.entity.Match;
import tennis.storage.XStreamMatchMarshaller;

public class XStreamMatchMarshallerTest {

	private final static XStreamMatchMarshaller MARSHALLER = new XStreamMatchMarshaller();
	private final static PointFactory POINT_FACTORY = new PointFactory();
	
	@Test
	public void testNewMatch() throws IllegalMoveException {
		Match match = new Match();
		match.setSetFactory(new SetFactory());
		match.start();
		match.add(POINT_FACTORY.wonPoint());
		String matchAsString = MARSHALLER.marshal(match);
		Match restoredMatch = MARSHALLER.unmarshal(matchAsString);
		Assert.assertEquals(match.getCurrentScore().getWon(), restoredMatch.getCurrentScore().getWon());
		Assert.assertEquals(match.getCurrentScore().getLost(), restoredMatch.getCurrentScore().getLost());
	}

	//@Test
	public void testXmlSize() throws IllegalMoveException {
		Match match = new Match();
		match.setSetFactory(new SetFactory());
		match.start();
		match.add(POINT_FACTORY.wonPoint());
		String matchAsString = MARSHALLER.marshal(match);
		System.out.println("Length after 1 point:" + matchAsString.length());
		match.add(POINT_FACTORY.wonPoint());
		match.add(POINT_FACTORY.wonPoint());
		match.add(POINT_FACTORY.wonPoint());
		match.add(POINT_FACTORY.wonPoint());
		matchAsString = MARSHALLER.marshal(match);
		System.out.println("Length in second game:" + matchAsString.length());
		match.add(POINT_FACTORY.wonPoint());
		match.add(POINT_FACTORY.wonPoint());
		match.add(POINT_FACTORY.wonPoint());
		matchAsString = MARSHALLER.marshal(match);
		System.out.println("Length in third game:" + matchAsString.length());
		
		try {
			OutputStream os = new FileOutputStream("/Users/amir/tmp/match.xml");
			os.write(matchAsString.getBytes("UTF-8"));
			os.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Match restoredMatch = MARSHALLER.unmarshal(matchAsString);
		Assert.assertEquals(match.getCurrentScore().getWon(), restoredMatch.getCurrentScore().getWon());
		Assert.assertEquals(match.getCurrentScore().getLost(), restoredMatch.getCurrentScore().getLost());
	}

}
