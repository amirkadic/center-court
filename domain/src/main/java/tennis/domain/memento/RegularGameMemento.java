/*******************************************************************************
 * Copyright (c) 2012 Amir Kadić.
 * 
 * This file is part of Center Court.
 * 
 * Center Court is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Center Court is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Center Court.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Contributors:
 *     Amir Kadić - initial API and implementation
 ******************************************************************************/
package tennis.domain.memento;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import tennis.domain.IllegalMoveException;
import tennis.domain.PlayGraphFactory;
import tennis.domain.WinStatus;
import tennis.domain.entity.Game;
import tennis.domain.entity.PlayElement;
import tennis.domain.entity.Point;
import tennis.domain.entity.RegularGame;
import tennis.domain.entity.Score;

public class RegularGameMemento extends GameMemento{

	private WinStatus status;
	private PlayElement playElement;
	private List<Point> points;
	private PlayGraphFactory playGraphFactory;
	private Date time;
	private boolean serve;
	
	public RegularGameMemento(WinStatus status, PlayElement playElement,
			List<Point> points, PlayGraphFactory playGraphFactory, Date time,
			boolean serve) {
		this.status = status;
		this.playElement = playElement;
		this.points = new ArrayList<Point>(points);
		this.playGraphFactory = playGraphFactory;
		this.time = time;
		this.serve = serve;
	}

	public Game restore() throws IllegalMoveException {
		return RegularGame.restore(this);	}
	
	public WinStatus getStatus() {
		return status;
	}

	public PlayElement getPlayElement() {
		return playElement;
	}

	public List<Point> getPoints() {
		return points;
	}

	public PlayGraphFactory getPlayGraphFactory() {
		return playGraphFactory;
	}

	public Date getTime() {
		return time;
	}

	public boolean isServe() {
		return serve;
	}

}
