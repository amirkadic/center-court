/*******************************************************************************
 * Copyright (c) 2012 Amir Kadić.
 * 
 * This file is part of Center Court.
 * 
 * Center Court is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Center Court is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Center Court.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Contributors:
 *     Amir Kadić - initial API and implementation
 ******************************************************************************/
package tennis.app.gui.activity;

import tennis.app.gui.dto.ParcelableMatchReference;
import tennis.storage.MatchStore;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;

public class DeleteMatchTask extends
		AsyncTask<ParcelableMatchReference, Void, ParcelableMatchReference> {

	private MatchStore matchStore;
	private ArrayAdapter<ParcelableMatchReference> listAdapter;

	public DeleteMatchTask(MatchStore matchStore, ArrayAdapter<ParcelableMatchReference> listAdapter) {
		super();
		this.matchStore = matchStore;
		this.listAdapter = listAdapter;
	}

	@Override
	protected ParcelableMatchReference doInBackground(ParcelableMatchReference... argList) {
		ParcelableMatchReference matchReference = argList[0];
		if (matchStore.remove(matchReference.getId())) {
			Log.d("MatchList", "Deleted match " + matchReference.getId() + ".");
		}
		return matchReference;
	}

	@Override
	protected void onPostExecute(ParcelableMatchReference matchReference) {
		listAdapter.remove(matchReference);
	}
	
	


}
