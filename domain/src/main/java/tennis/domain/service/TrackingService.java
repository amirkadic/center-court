/*******************************************************************************
 * Copyright (c) 2012 Amir Kadić.
 * 
 * This file is part of Center Court.
 * 
 * Center Court is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Center Court is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Center Court.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Contributors:
 *     Amir Kadić - initial API and implementation
 ******************************************************************************/
package tennis.domain.service;

import tennis.domain.IllegalMoveException;
import tennis.domain.entity.Match;
import tennis.domain.value.MatchScore;
import tennis.domain.value.ScoreInSets;

public interface TrackingService {

	/**
	 * Starts a new match. Warning: any previously started match will be dropped
	 * and its data lost!
	 */
	void start();

	/**
	 * Resumes a match previously started.
	 * 
	 * @param match
	 *            The match previously started.
	 */
	void resume(Match match);

	/**
	 * Track a point that was won.
	 * 
	 * @throws IllegalMoveException
	 */
	void winPoint() throws IllegalMoveException;

	/**
	 * Track a point that was lost.
	 * 
	 * @throws IllegalMoveException
	 */
	void losePoint() throws IllegalMoveException;

	/**
	 * Checks if the match is already finished.
	 * 
	 * @return true if the match is finished, otherwise false.
	 */
	boolean matchOver();

	/**
	 * Access to the match being tracked.
	 * 
	 * @return The match being tracked.
	 */
	Match getMatch();

	/**
	 * Obtain the current score.
	 * 
	 * @return The current full match score.
	 */
	MatchScore currentScore();

	/**
	 * Obtain the current score in sets.
	 * 
	 * @return The current score in sets.
	 */
	ScoreInSets currentScoreInSets();

	/**
	 * Stop tracking the match, if one is being tracked.
	 */
	void stop();

	/**
	 * Check if a match is being tracked.
	 * 
	 * @return true if a match is being tracked, otherwise false.
	 */
	boolean isRunning();

	/**
	 * Undoes the last point tracked.
	 * 
	 * @throws IllegalMoveException
	 */
	void undo() throws IllegalMoveException;

	/**
	 * Check if the last point tracked (if any) can be undone.
	 */
	boolean canUndo();

	/**
	 * Jump to an explicit score down the road.
	 * 
	 * @param targetScore
	 *            The score to jump to.
	 * @throws IllegalMoveException
	 *             If the score can't possibly come after the current score and
	 *             it wasn't found in the undo history.
	 */
	void jumpTo(ScoreInSets targetScore) throws IllegalMoveException;

	/**
	 * Adds a comment to the last point played. If there are no points yet, it
	 * has no effect. If the point already has a comment, it is overwritten.
	 * 
	 * @param text
	 *            The comment text.
	 */
	void commentLastPoint(String text);

	/**
	 * Explicitly pause tracking the current match, persisting its state
	 * immediately.
	 */
	void pause();

	/**
	 * Finish tracking this match, regardless of score and/or status.
	 */
	void finishTracking();
}
