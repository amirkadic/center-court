/*******************************************************************************
 * Copyright (c) 2012 Amir Kadić.
 * 
 * This file is part of Center Court.
 * 
 * Center Court is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Center Court is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Center Court.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Contributors:
 *     Amir Kadić - initial API and implementation
 ******************************************************************************/
package tennis.web.serialization;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import tennis.domain.Handedness;
import tennis.domain.Surface;
import tennis.domain.WinStatus;
import tennis.domain.entity.Score;

import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.ConverterLookup;
import com.thoughtworks.xstream.converters.SingleValueConverterWrapper;
import com.thoughtworks.xstream.converters.enums.EnumConverter;

public class CenterCourtConverterLookup implements ConverterLookup {

	private ConverterLookup basic;
	private Map<Class<?>, Converter> customConverters = new HashMap<Class<?>, Converter>();

	public CenterCourtConverterLookup(ConverterLookup basic) {
		this.basic = basic;
		this.customConverters.put(Date.class, new SingleValueConverterWrapper(
				new DateConverter()));
		this.customConverters.put(Score.class, new SingleValueConverterWrapper(
				new ScoreConverter()));
		EnumConverter enumConverter = new EnumConverter();
		this.customConverters.put(WinStatus.class, enumConverter);
		this.customConverters.put(Handedness.class, enumConverter);
		this.customConverters.put(Surface.class, enumConverter);
	}

	public Converter lookupConverterForType(
			@SuppressWarnings("rawtypes") Class klass) {
		if (this.customConverters.containsKey(klass)) {
			return this.customConverters.get(klass);
		} else {
			return basic.lookupConverterForType(klass);
		}
	}
}
