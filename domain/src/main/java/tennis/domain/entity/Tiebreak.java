/*******************************************************************************
 * Copyright (c) 2012 Amir Kadić.
 * 
 * This file is part of Center Court.
 * 
 * Center Court is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Center Court is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Center Court.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Contributors:
 *     Amir Kadić - initial API and implementation
 ******************************************************************************/
package tennis.domain.entity;

import java.util.Date;
import java.util.List;

import tennis.domain.IllegalMoveException;
import tennis.domain.PlayGraphFactory;
import tennis.domain.WinStatus;
import tennis.domain.memento.GameMemento;
import tennis.domain.memento.TiebreakMemento;

public class Tiebreak extends Game {

	private CountableScore score;

	@Override
	public void start() {
		this.status = WinStatus.PENDING;
		this.time = new Date();
		this.score = new CountableScore(0, 0);
	}

	public Tiebreak() {}
	
	public Tiebreak(long id, WinStatus status, Date time, boolean serve,
			CountableScore score) {
		this.id = id;
		this.status = status;
		this.time = time;
		this.serve = serve;
		this.score = score;
	}

	@Override
	public void add(Point point) throws IllegalMoveException {
		if (this.status != WinStatus.PENDING) {
			throw new IllegalMoveException(
					"Can't add a point to a tiebreak that was already "
							+ this.status + ".");
		}
		this.score = this.score.count(point.getStatus());
		this.updateStatus();
		this.points.add(point);
		// how cool is this? should the point know what game score
		// it produced? this can be kept in a separate "point2Score"
		// map so the point is kept clean.
		point.setScore(this.score);
	}

	@Override
	public GameMemento saveToMemento() {
		return new TiebreakMemento(this.status, points, time, serve,
				score);
	}

	public static Game restore(TiebreakMemento memento) {
		Tiebreak tiebreak = new Tiebreak();
		tiebreak.points = memento.getPoints();
		tiebreak.status = memento.getStatus();
		tiebreak.time = memento.getTime();
		tiebreak.serve = memento.isServe();
		tiebreak.score = memento.getScore();
		return tiebreak;
	}

	@Override
	public Score getScore() {
		return this.score;
	}

	private void validate(CountableScore score) {
		short won = score.getWon();
		short lost = score.getLost();
		if (Math.abs(won - lost) > 7
				|| (Math.min(won, lost) > 4 && Math.abs(won - lost) > 2)) {
			throw new IllegalArgumentException(score
					+ " is no valid score for a tiebreak.");
		}
	}

	private void updateStatus() {
		short won = this.score.getWon();
		short lost = this.score.getLost();
		short difference = (short) (won - lost);
		if (Math.max(won, lost) > 6 && Math.abs(difference) > 1) {
			this.status = difference > 0 ? WinStatus.WON : WinStatus.LOST;
		}

	}
}
