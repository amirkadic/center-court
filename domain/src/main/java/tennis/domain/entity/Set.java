/*******************************************************************************
 * Copyright (c) 2012 Amir Kadić.
 * 
 * This file is part of Center Court.
 * 
 * Center Court is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Center Court is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Center Court.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Contributors:
 *     Amir Kadić - initial API and implementation
 ******************************************************************************/
package tennis.domain.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamImplicit;

import tennis.domain.GameFactory;
import tennis.domain.IllegalMoveException;
import tennis.domain.WinStatus;
import tennis.domain.memento.GameMemento;
import tennis.domain.memento.SetMemento;

public class Set {

	// state
	@XStreamImplicit(itemFieldName = "game")
	private List<Game> games = new ArrayList<Game>();
	private CountableScore currentScore;
	private WinStatus status = WinStatus.PENDING;

	// immutable
	private GameFactory gameFactory;
	private Date time;
	private long id;
	
	public Set() {
	}
	
	public Set(long id, Date time, CountableScore score, WinStatus winStatus, List<Game> games, GameFactory gameFactory) {
		this.id = id;
		this.time = time;
		this.currentScore = score;
		this.status = winStatus;
		this.games = games;
		this.gameFactory = gameFactory;
	}

	public void start() {
		this.currentScore = new CountableScore((short) 0, (short) 0);
		this.games.add(this.gameFactory.startNewGame());
		this.time = new Date();
	}

	public void add(Point point) throws IllegalMoveException {

		if (this.status != WinStatus.PENDING) {
			throw new IllegalMoveException(
					"Can't add a point to a set that was already "
							+ this.status + ".");
		}
		Game game = this.currentGame();
		game.add(point);
		if (game.getStatus() != WinStatus.PENDING) {
			this.currentScore = this.currentScore.count(game.getStatus());
			this.updateStatusOrStartNewGame();
		}

	}

	public CountableScore getCurrentScore() {
		return currentScore;
	}

	public WinStatus getStatus() {
		return status;
	}

	public Game getCurrentGame() {
		return this.games.get(this.games.size() - 1);
	}

	public void setGameFactory(GameFactory gameFactory) {
		this.gameFactory = gameFactory;
	}

	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	static Set restore(SetMemento memento) throws IllegalMoveException {
		Set set = new Set();
		set.currentScore = memento.getScore();
		set.status = memento.getStatus();
		set.games = new ArrayList<Game>();
		for (GameMemento gameMemento : memento.getGameMementos()) {
			set.games.add(gameMemento.restore());
		}
		set.gameFactory = memento.getGameFactory();
		set.time = memento.getTime();
		return set;
	}

	SetMemento saveToMemento() {
		List<GameMemento> gameMementos = new ArrayList<GameMemento>();
		for (Game game : this.games) {
			gameMementos.add(game.saveToMemento());
		}
		return new SetMemento(this.status, new CountableScore(
				this.currentScore.getWon(), this.currentScore.getLost()),
				gameMementos, this.time, this.gameFactory);
	}

	public void fastForwardTo(CountableScore games)
			throws IllegalMoveException {
		this.currentScore = games;
		// FIXME maybe this call shouldn't happen if there is no games score
		// change!
		// the idea is that no new game should started if we don't actually jump
		// but
		// keep the existing games score.
		// this seems to be enforced by the canFollow check however so the new
		// games score is always "after" the current.
		this.updateStatusOrStartNewGame();
	}

	public void finishWith(CountableScore score) {
		this.status = score.getWon() > score.getLost() ? WinStatus.WON
				: WinStatus.LOST;
		this.currentScore = score;

	}

	public List<Game> getGames() {
		return this.games;
	}
	
	private Game currentGame() {
		return this.games.get(games.size() - 1);
	}

	private void updateStatusOrStartNewGame() {
		int won = this.currentScore.getWon();
		int lost = this.currentScore.getLost();
		// TODO delegate this to an abstract set strategy to support
		// tie-breaks
		if ((Math.max(won, lost) == 6 && Math.abs(won - lost) > 1)
				|| (Math.max(won, lost) == 7)) {
			// the set was decided
			this.status = won > lost ? WinStatus.WON : WinStatus.LOST;
		} else if (won == 6 && lost == 6) {
			this.games.add(this.gameFactory.startTiebreak());
		} else {

			this.games.add(this.gameFactory.startNewGame());
		}
	}

}
