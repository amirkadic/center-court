/*******************************************************************************
 * Copyright (c) 2012 Amir Kadić.
 * 
 * This file is part of Center Court.
 * 
 * Center Court is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Center Court is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Center Court.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Contributors:
 *     Amir Kadić - initial API and implementation
 ******************************************************************************/
package tennis.web.serialization;

import java.io.OutputStream;

import tennis.domain.entity.Game;
import tennis.domain.entity.Match;
import tennis.domain.entity.Player;
import tennis.domain.entity.Point;
import tennis.domain.entity.RegularGame;
import tennis.domain.entity.Set;
import tennis.domain.entity.Tiebreak;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.converters.ConverterRegistry;
import com.thoughtworks.xstream.converters.reflection.FieldDictionary;
import com.thoughtworks.xstream.converters.reflection.PureJavaReflectionProvider;
import com.thoughtworks.xstream.converters.reflection.ReflectionProvider;
import com.thoughtworks.xstream.converters.reflection.SortableFieldKeySorter;
import com.thoughtworks.xstream.core.JVM;
import com.thoughtworks.xstream.io.HierarchicalStreamDriver;
import com.thoughtworks.xstream.io.xml.DomDriver;
import com.thoughtworks.xstream.mapper.AnnotationMapper;

public class XmlMatchSerializer implements MatchSerializer {

	private XStream xStream;

	public XmlMatchSerializer() {
		HierarchicalStreamDriver driver = new DomDriver("UTF-8");
		xStream = new XStream(driver);
		JVM jvm = new JVM();
//		AnnotationMapper annotationMapper = new AnnotationMapper(
//				xStream.getMapper(),
//				(ConverterRegistry) xStream.getConverterLookup(),
//				xStream.getClassLoader(), xStream.getReflectionProvider(), jvm);
		AnnotationMapper annotationMapper = new AnnotationMapper(
				xStream.getMapper(),
				(ConverterRegistry) xStream.getConverterLookup(),xStream.getConverterLookup(),
				xStream.getClassLoader(), xStream.getReflectionProvider(), jvm);
		SortableFieldKeySorter sorter = new SortableFieldKeySorter();
		sorter.registerFieldOrder(Match.class, new String[] { "player",
				"opponent", "sets", "xmlns", "currentScore", "status", "date",
				"serveFirst", "setFactory", "time", "trackingStatus", "id", "surface", "note" });
//		ReflectionProvider reflectionProvider = new Sun14ReflectionProvider(
//				new FieldDictionary(sorter));
		ReflectionProvider reflectionProvider = new PureJavaReflectionProvider(
				new FieldDictionary(sorter));
		
		xStream = new XStream(reflectionProvider, driver,
				xStream.getClassLoader(), annotationMapper,
				new CenterCourtConverterLookup(xStream.getConverterLookup()),
				(ConverterRegistry) xStream.getConverterLookup());

		xStream.registerConverter(new EnumConverter());
		//
		xStream.registerConverter(new DateConverter());
		xStream.registerConverter(new ScoreConverter());
		xStream.registerConverter(new GameConverter(xStream.getConverterLookup().lookupConverterForType(Game.class), reflectionProvider));
		xStream.processAnnotations(new Class[] { Match.class, Set.class,
				Game.class });
		xStream.omitField(Match.class, "setFactory");
		xStream.omitField(Match.class, "time");
		xStream.omitField(Match.class, "trackingStatus");
		xStream.alias("match", Match.class);
		xStream.aliasAttribute(Match.class, "currentScore", "score");
		xStream.aliasAttribute(Match.class, "serveFirst", "serve-first");
		xStream.aliasAttribute(Match.class, "date", "time");
		xStream.useAttributeFor(Match.class, "status");
		xStream.useAttributeFor(Match.class, "date");
		xStream.useAttributeFor(Match.class, "xmlns");

		xStream.alias("set", Set.class);
		xStream.aliasAttribute(Set.class, "currentScore", "score");
		xStream.omitField(Set.class, "gameFactory");
		xStream.useAttributeFor(Set.class, "status");
		xStream.useAttributeFor(Set.class, "currentScore");
		xStream.useAttributeFor(Set.class, "time");

//		xStream.omitField(Game.class, "playGraphFactory");
//		xStream.omitField(Game.class, "playGraph");
		xStream.useAttributeFor(Game.class, "status");
//		xStream.useAttributeFor(Game.class, "score");
		xStream.useAttributeFor(Game.class, "time");
		xStream.useAttributeFor(Game.class, "serve");

		xStream.alias("game", RegularGame.class);
		xStream.omitField(RegularGame.class, "playGraphFactory");
		xStream.omitField(RegularGame.class, "playGraph");
//		xStream.useAttributeFor(RegularGame.class, "status");
		xStream.useAttributeFor(RegularGame.class, "score");
//		xStream.useAttributeFor(RegularGame.class, "time");
//		xStream.useAttributeFor(RegularGame.class, "serve");

		xStream.alias("game", Tiebreak.class);
		xStream.omitField(Tiebreak.class, "playGraphFactory");
		xStream.omitField(Tiebreak.class, "playGraph");
//		xStream.useAttributeFor(Tiebreak.class, "status");
		xStream.useAttributeFor(Tiebreak.class, "score");
//		xStream.useAttributeFor(Tiebreak.class, "time");
//		xStream.useAttributeFor(Tiebreak.class, "serve");
		
		xStream.addDefaultImplementation(RegularGame.class, Game.class);
		xStream.addDefaultImplementation(Tiebreak.class, Game.class);

		xStream.alias("point", Point.class);
		xStream.useAttributeFor(Point.class, "status");
		xStream.useAttributeFor(Point.class, "score");
		xStream.useAttributeFor(Point.class, "date");
		xStream.aliasAttribute(Point.class, "score", "game-score");
		xStream.aliasAttribute(Point.class, "date", "time");

		xStream.aliasAttribute(Match.class, "player", "player");
		xStream.aliasAttribute(Match.class, "opponent", "opponent");
		xStream.useAttributeFor(Player.class, "name");
		xStream.useAttributeFor(Player.class, "age");
		xStream.useAttributeFor(Player.class, "handedness");
		xStream.useAttributeFor(Player.class, "surface");
		xStream.aliasAttribute(Player.class, "handedness", "hand");
	}

	public void serialize(Match match, OutputStream os) {
//		String debug = this.xStream.toXML(match);
//		System.out.print(debug);
//		Log.d("XmlMatchSerializer", debug);
		this.xStream.toXML(match, os);
	}

}
