/*******************************************************************************
 * Copyright (c) 2012 Amir Kadić.
 * 
 * This file is part of Center Court.
 * 
 * Center Court is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Center Court is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Center Court.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Contributors:
 *     Amir Kadić - initial API and implementation
 ******************************************************************************/
package tennis.web.service;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import tennis.domain.entity.Match;
import tennis.web.service.converter.MatchMessageConverter;

/**
 * 
 * @author amir
 * 
 */
public class RestfulMatchService implements MatchService {

	private static MatchService singleton;
	private URI url;
	private RestTemplate restTemplate;

	public static synchronized MatchService getService() {
		if (RestfulMatchService.singleton == null) {
			RestfulMatchService.singleton = new RestfulMatchService();
		}
		return RestfulMatchService.singleton;
	}

	private RestfulMatchService() {
		List<HttpMessageConverter<?>> converters = new ArrayList<HttpMessageConverter<?>>();
		converters.add(MatchMessageConverter.getConverter());
		this.restTemplate = new RestTemplate();
		this.restTemplate.setMessageConverters(converters);
		try {
			this.url = new URI("http://www.center-court.net/repository/matches/");
		} catch (URISyntaxException e) {
			throw new RuntimeException("Bad REST service URI.", e);
		}
	}

	public void upload(Match match) {
		this.restTemplate.postForLocation(this.url, match);
	}

}
