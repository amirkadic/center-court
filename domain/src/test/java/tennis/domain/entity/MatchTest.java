/*******************************************************************************
 * Copyright (c) 2012 Amir Kadić.
 * 
 * This file is part of Center Court.
 * 
 * Center Court is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Center Court is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Center Court.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Contributors:
 *     Amir Kadić - initial API and implementation
 ******************************************************************************/
package tennis.domain.entity;

import java.util.Arrays;
import java.util.Date;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import tennis.domain.GameFactory;
import tennis.domain.IllegalMoveException;
import tennis.domain.MatchFactory;
import tennis.domain.SetFactory;
import tennis.domain.WinStatus;
import tennis.domain.entity.CountableScore;
import tennis.domain.entity.Match;
import tennis.domain.entity.Point;
import tennis.domain.entity.Tiebreak;
import tennis.domain.memento.MatchMemento;
import tennis.domain.value.BestOf3ScoreInSets;
import tennis.domain.value.MatchScore;

public class MatchTest {

	private static final MatchFactory matchFactory = new MatchFactory();

	@BeforeClass
	public static void setup() {
		SetFactory setFactory = new SetFactory();
	}

	@Test
	public void straightWin() throws IllegalMoveException {

		Match match = MatchTest.matchFactory.startNewMatch();

		for (int i = 0; i < 48; i++) {
			Point winner = new Point();
			winner.setStatus(WinStatus.WON);
			winner.setDate(new Date());
			match.add(winner);
		}
		Assert.assertEquals(WinStatus.WON, match.getStatus());
		Assert.assertEquals(new CountableScore(2, 0), match.getCurrentScore());
	}

	@Test
	public void straightLoss() throws IllegalMoveException {

		Match match = MatchTest.matchFactory.startNewMatch();

		for (int i = 0; i < 48; i++) {
			Point winner = new Point();
			winner.setStatus(WinStatus.LOST);
			winner.setDate(new Date());
			match.add(winner);
		}
		Assert.assertEquals(WinStatus.LOST, match.getStatus());
		Assert.assertEquals(new CountableScore(0, 2), match.getCurrentScore());
	}

	@Test(expected = IllegalMoveException.class)
	public void illegalPoint() throws IllegalMoveException {

		Match match = MatchTest.matchFactory.startNewMatch();

		for (int i = 0; i < 72; i++) {
			Point winner = new Point();
			winner.setStatus(WinStatus.WON);
			winner.setDate(new Date());
			match.add(winner);
		}
		Assert.assertEquals(WinStatus.WON, match.getStatus());
		Assert.assertEquals(new CountableScore(3, 0), match.getCurrentScore());
		// playing point in finished match
		Point illegalPoint = new Point();
		illegalPoint.setStatus(WinStatus.WON);
		illegalPoint.setDate(new Date());
		match.add(illegalPoint);

	}

	@Test
	public void skipOnePoint() throws IllegalMoveException {

		Match match = MatchTest.matchFactory.startNewMatch();

		for (int i = 0; i < 46; i++) {
			Point winner = new Point();
			winner.setStatus(WinStatus.WON);
			winner.setDate(new Date());
			match.add(winner);
		}
		Assert.assertEquals(WinStatus.PENDING, match.getStatus());
		Assert.assertEquals(new CountableScore(1, 0), match.getCurrentScore());
		match.fastForwardTo(new BestOf3ScoreInSets(Arrays.asList(
				new CountableScore(6, 0), new CountableScore(6, 0))));
		Assert.assertEquals(new CountableScore(2, 0), match.getCurrentScore());
		Assert.assertEquals(WinStatus.WON, match.getStatus());

	}

	@Test
	public void skipAGame() throws IllegalMoveException {

		Match match = MatchTest.matchFactory.startNewMatch();

		for (int i = 0; i < 41; i++) {
			Point winner = new Point();
			winner.setStatus(WinStatus.WON);
			winner.setDate(new Date());
			match.add(winner);
		}
		Assert.assertEquals(WinStatus.PENDING, match.getStatus());
		Assert.assertEquals(new CountableScore(1, 0), match.getCurrentScore());
		match.fastForwardTo(new BestOf3ScoreInSets(Arrays.asList(
				new CountableScore(6, 0), new CountableScore(5, 0))));
		Assert.assertEquals(WinStatus.PENDING, match.getStatus());
		Assert.assertEquals(new CountableScore(1, 0), match.getCurrentScore());
		Assert.assertEquals(
				new MatchScore(new BestOf3ScoreInSets(Arrays.asList(
						new CountableScore(6, 0), new CountableScore(5, 0))),
						new CountableScore(0, 0)), match.currentScore());
		for (int i = 0; i < 4; i++) {
			Point winner = new Point();
			winner.setStatus(WinStatus.WON);
			winner.setDate(new Date());
			match.add(winner);
		}
		Assert.assertEquals(WinStatus.WON, match.getStatus());
		Assert.assertEquals(new CountableScore(2, 0), match.getCurrentScore());

	}

	@Test
	public void skipASetAndUseMemento() throws IllegalMoveException {

		Match match = MatchTest.matchFactory.startNewMatch();

		for (int i = 0; i < 20; i++) {
			Point winner = new Point();
			winner.setStatus(WinStatus.WON);
			winner.setDate(new Date());
			match.add(winner);
		}
		Assert.assertEquals(WinStatus.PENDING, match.getStatus());
		Assert.assertEquals(
				new MatchScore(new BestOf3ScoreInSets(Arrays
						.asList(new CountableScore(5, 0))), new CountableScore(
						0, 0)), match.currentScore());
		Assert.assertEquals(new CountableScore(0, 0), match.getCurrentScore());
		MatchMemento memento = match.saveToMemento();
		match.fastForwardTo(new BestOf3ScoreInSets(Arrays.asList(
				new CountableScore(6, 0), new CountableScore(4, 0))));
		MatchMemento afterFirstJump = match.saveToMemento();
		Assert.assertEquals(WinStatus.PENDING, match.getStatus());
		Assert.assertEquals(new CountableScore(1, 0), match.getCurrentScore());
		Assert.assertEquals(
				new MatchScore(new BestOf3ScoreInSets(Arrays.asList(
						new CountableScore(6, 0), new CountableScore(4, 0))),
						new CountableScore(0, 0)), match.currentScore());
		for (int i = 0; i < 8; i++) {
			Point winner = new Point();
			winner.setStatus(WinStatus.WON);
			winner.setDate(new Date());
			match.add(winner);
		}
		Assert.assertEquals(WinStatus.WON, match.getStatus());
		Assert.assertEquals(new CountableScore(2, 0), match.getCurrentScore());
		match.restore(memento);
		Assert.assertEquals(WinStatus.PENDING, match.getStatus());
		Assert.assertEquals(
				new MatchScore(new BestOf3ScoreInSets(Arrays
						.asList(new CountableScore(5, 0))), new CountableScore(
						0, 0)), match.currentScore());
		Assert.assertEquals(new CountableScore(0, 0), match.getCurrentScore());
		match.fastForwardTo(new BestOf3ScoreInSets(Arrays.asList(
				new CountableScore(6, 0), new CountableScore(4, 0))));
		Assert.assertEquals(WinStatus.PENDING, match.getStatus());
		Assert.assertEquals(new CountableScore(1, 0), match.getCurrentScore());
		Assert.assertEquals(
				new MatchScore(new BestOf3ScoreInSets(Arrays.asList(
						new CountableScore(6, 0), new CountableScore(4, 0))),
						new CountableScore(0, 0)), match.currentScore());
		for (int i = 0; i < 8; i++) {
			Point winner = new Point();
			winner.setStatus(WinStatus.WON);
			winner.setDate(new Date());
			match.add(winner);
		}
		Assert.assertEquals(WinStatus.WON, match.getStatus());
		Assert.assertEquals(new CountableScore(2, 0), match.getCurrentScore());
		match.restore(afterFirstJump);
		Assert.assertEquals(WinStatus.PENDING, match.getStatus());
		Assert.assertEquals(new CountableScore(1, 0), match.getCurrentScore());
		Assert.assertEquals(
				new MatchScore(new BestOf3ScoreInSets(Arrays.asList(
						new CountableScore(6, 0), new CountableScore(4, 0))),
						new CountableScore(0, 0)), match.currentScore());
		for (int i = 0; i < 8; i++) {
			Point winner = new Point();
			winner.setStatus(WinStatus.WON);
			winner.setDate(new Date());
			match.add(winner);
		}
		Assert.assertEquals(WinStatus.WON, match.getStatus());
		Assert.assertEquals(new CountableScore(2, 0), match.getCurrentScore());

	}

	@Test
	public void tiebreak() throws IllegalMoveException {
		Match match = MatchTest.matchFactory.startNewMatch();
		// win one set
		for (int i = 0; i < 24; i++) {
			Point winner = new Point();
			winner.setStatus(WinStatus.WON);
			winner.setDate(new Date());
			match.add(winner);
		}
		// win second set in a tiebreak
		// first, produce 5:0
		for (int i = 0; i < 20; i++) {
			Point winner = new Point();
			winner.setStatus(WinStatus.WON);
			winner.setDate(new Date());
			match.add(winner);
		}
		// then 5:6
		for (int i = 0; i < 24; i++) {
			Point loser = new Point();
			loser.setStatus(WinStatus.LOST);
			loser.setDate(new Date());
			match.add(loser);
		}
		// then 6:6
		for (int i = 0; i < 4; i++) {
			Point winner = new Point();
			winner.setStatus(WinStatus.WON);
			winner.setDate(new Date());
			match.add(winner);
		}
		// win one point in the tiebreak
		for (int i = 0; i < 1; i++) {
			Point winner = new Point();
			winner.setStatus(WinStatus.WON);
			winner.setDate(new Date());
			match.add(winner);
		}
		// make sure we are in a tiebreak
		Assert.assertTrue(match.getCurrentSet().getCurrentGame() instanceof Tiebreak);
		// win tiebreak (and thus match)
		for (int i = 0; i < 6; i++) {
			Point winner = new Point();
			winner.setStatus(WinStatus.WON);
			winner.setDate(new Date());
			match.add(winner);
		}
		Assert.assertEquals(WinStatus.WON, match.getStatus());
		Assert.assertEquals(new CountableScore(2, 0), match.getCurrentScore());
	}

	@Test
	public void jumpIntoTiebreak() throws IllegalMoveException {

		Match match = MatchTest.matchFactory.startNewMatch();

		for (int i = 0; i < 41; i++) {
			Point winner = new Point();
			winner.setStatus(WinStatus.WON);
			winner.setDate(new Date());
			match.add(winner);
		}
		Assert.assertEquals(WinStatus.PENDING, match.getStatus());
		Assert.assertEquals(new CountableScore(1, 0), match.getCurrentScore());
		match.fastForwardTo(new BestOf3ScoreInSets(Arrays.asList(
				new CountableScore(6, 0), new CountableScore(6, 6))));
		Assert.assertEquals(WinStatus.PENDING, match.getStatus());
		Assert.assertEquals(new CountableScore(1, 0), match.getCurrentScore());
		Assert.assertEquals(
				new MatchScore(new BestOf3ScoreInSets(Arrays.asList(
						new CountableScore(6, 0), new CountableScore(6, 6))),
						new CountableScore(0, 0)), match.currentScore());
		Assert.assertTrue(match.getCurrentSet().getCurrentGame() instanceof Tiebreak);
		for (int i = 0; i < 7; i++) {
			Point winner = new Point();
			winner.setStatus(WinStatus.WON);
			winner.setDate(new Date());
			match.add(winner);
		}
		Assert.assertEquals(WinStatus.WON, match.getStatus());
		Assert.assertEquals(new CountableScore(2, 0), match.getCurrentScore());

	}

	@Test(expected = IllegalMoveException.class)
	public void invalidJump() throws IllegalMoveException {

		Match match = MatchTest.matchFactory.startNewMatch();

		for (int i = 0; i < 41; i++) {
			Point winner = new Point();
			winner.setStatus(WinStatus.WON);
			winner.setDate(new Date());
			match.add(winner);
		}
		Assert.assertEquals(WinStatus.PENDING, match.getStatus());
		Assert.assertEquals(new CountableScore(1, 0), match.getCurrentScore());
		match.fastForwardTo(new BestOf3ScoreInSets(Arrays.asList(
				new CountableScore(6, 1), new CountableScore(5, 0))));

	}

	@Test(expected = IllegalMoveException.class)
	public void invalidJumpSevenSeven() throws IllegalMoveException {

		Match match = MatchTest.matchFactory.startNewMatch();

		Assert.assertEquals(WinStatus.PENDING, match.getStatus());
		Assert.assertEquals(new CountableScore(0, 0), match.getCurrentScore());
		match.fastForwardTo(new BestOf3ScoreInSets(Arrays
				.asList(new CountableScore(7, 7))));

	}

	@Test(expected = IllegalMoveException.class)
	public void invalidJumpFirstsetUnfinishedButJumpingintoSecond()
			throws IllegalMoveException {

		Match match = MatchTest.matchFactory.startNewMatch();

		Assert.assertEquals(WinStatus.PENDING, match.getStatus());
		Assert.assertEquals(new CountableScore(0, 0), match.getCurrentScore());
		match.fastForwardTo(new BestOf3ScoreInSets(Arrays.asList(
				new CountableScore(2, 0), new CountableScore(2, 0))));

	}

}
