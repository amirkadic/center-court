/*******************************************************************************
 * Copyright (c) 2012 Amir Kadić.
 * 
 * This file is part of Center Court.
 * 
 * Center Court is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Center Court is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Center Court.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Contributors:
 *     Amir Kadić - initial API and implementation
 ******************************************************************************/
package tennis.web.serialization;

import java.util.Map;

import tennis.domain.entity.Game;
import tennis.domain.entity.RegularGame;
import tennis.domain.entity.Tiebreak;

import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.converters.reflection.ReflectionProvider;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;

public class GameConverter implements Converter{
	
	private Converter defaultConverter;
	private ReflectionProvider reflectionProvider;
	
	public GameConverter(Converter defaultConverter,
			ReflectionProvider reflectionProvider) {
		this.defaultConverter = defaultConverter;
		this.reflectionProvider = reflectionProvider;
	}

	public boolean canConvert(Class klass) {
		return Game.class.isAssignableFrom(klass);
	}

	public void marshal(Object object, HierarchicalStreamWriter writer,
			MarshallingContext context) {
		String tiebreak = "false";
		if (object instanceof Tiebreak) {
			tiebreak = "true";
		}
		writer.addAttribute("tiebreak", tiebreak);
		this.defaultConverter.marshal(object, writer, context);
	}

	public Object unmarshal(HierarchicalStreamReader reader,
			UnmarshallingContext context) {
		String tiebreak = reader.getAttribute("tiebreak");
		Class resultClass = null;
		if (tiebreak!=null && tiebreak.equals("true")) {
			resultClass = Tiebreak.class;
		} else {
			resultClass = RegularGame.class;
		}
		Object result = this.reflectionProvider.newInstance(resultClass);
		return context.convertAnother(result, resultClass, this.defaultConverter);
	}

}
