/*******************************************************************************
 * Copyright (c) 2012 Amir Kadić.
 * 
 * This file is part of Center Court.
 * 
 * Center Court is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Center Court is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Center Court.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Contributors:
 *     Amir Kadić - initial API and implementation
 ******************************************************************************/
package tennis.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

public class OpenHelper extends SQLiteOpenHelper {

	public static final String DB_NAME = "CCDB";
	public static final int DB_VERSION = 1;

	// private constants
	public static final String MATCH_TABLE = "MATCH";
	public static final String MATCH_COLUMN_ID = "ID";
	public static final String MATCH_COLUMN_WINSTATUS = "WINSTATUS";
	public static final String MATCH_COLUMN_TRACKINGSTATUS = "TRACKINGSTATUS";
	public static final String MATCH_COLUMN_SCORE = "SCORE";
	public static final String MATCH_COLUMN_TIME = "TIME";
	public static final String MATCH_COLUMN_SERVEFIRST = "SERVEFIRST";
	public static final String MATCH_COLUMN_PLAYERID = "PLAYERID";
	public static final String MATCH_COLUMN_OPPONENTID = "OPPONENTID";
	public static final String MATCH_COLUMN_SURFACE = "SURFACE";
	public static final String MATCH_COLUMN_NOTE = "NOTE";
	public static final String SET_TABLE = "TENNIS_SET";
	public static final String SET_COLUMN_ID = "ID";
	public static final String SET_COLUMN_MATCH_ID = "MATCHID";
	public static final String SET_COLUMN_TIME = "TIME";
	public static final String SET_COLUMN_WINSTATUS = "WINSTATUS";
	public static final String SET_COLUMN_SCORE = "SCORE";
	public static final String GAME_TABLE = "GAME";
	public static final String GAME_COLUMN_ID = "ID";
	public static final String GAME_COLUMN_SET_ID = "SETID";
	public static final String GAME_COLUMN_TYPE = "TYPE";
	public static final String GAME_COLUMN_WINSTATUS = "WINSTATUS";
	public static final String GAME_COLUMN_TIME = "TIME";
	public static final String GAME_COLUMN_SERVE = "SERVE";
	public static final String GAME_COLUMN_SCORE = "SCORE";
	public static final String POINT_TABLE = "POINT";
	public static final String POINT_COLUMN_ID = "ID";
	public static final String POINT_COLUMN_GAME_ID = "GAMEID";
	public static final String POINT_COLUMN_WINSTATUS = "WINSTATUS";
	public static final String POINT_COLUMN_TIME = "TIME";
	public static final String POINT_COLUMN_COMMENT = "COMMENT";
	public static final String POINT_COLUMN_SCORE = "SCORE";
	public static final String PLAYER_TABLE = "PLAYER";
	public static final String PLAYER_COLUMN_ID = "ID";
	public static final String PLAYER_COLUMN_NAME = "NAME";

	private static OpenHelper store;

	/**
	 * Singleton factory method.
	 * @param context Android context.
	 * @return The OpenHelper singleton, which can be used to access the CC DB.
	 */
	public static synchronized OpenHelper getStore(Context context) {
		if (OpenHelper.store == null) {
			OpenHelper.store = new OpenHelper(context, DB_NAME, null,
					DB_VERSION);
		}
		return OpenHelper.store;
	}

	private OpenHelper(Context context, String name, CursorFactory factory,
			int version) {
		super(context, name, factory, version);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		this.createTable(db, POINT_TABLE, //
				POINT_COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT",//
				POINT_COLUMN_GAME_ID + " INTEGER REFERENCES " + GAME_TABLE
						+ "(" + GAME_COLUMN_ID + ") ON DELETE CASCADE",//
				POINT_COLUMN_COMMENT + " TEXT", //
				POINT_COLUMN_SCORE + " TEXT", //
				POINT_COLUMN_TIME + " INTEGER", //
				POINT_COLUMN_WINSTATUS + " TEXT");
		this.createTable(db, GAME_TABLE, //
				GAME_COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT", //
				GAME_COLUMN_SET_ID + " INTEGER REFERENCES " + SET_TABLE + "("
						+ SET_COLUMN_ID + ") ON DELETE CASCADE",//
				GAME_COLUMN_SCORE + " TEXT", //
				GAME_COLUMN_SERVE + " INTEGER", //
				GAME_COLUMN_TIME + " INTEGER", //
				GAME_COLUMN_TYPE + " INTEGER", //
				GAME_COLUMN_WINSTATUS + " INTEGER");
		this.createTable(db, SET_TABLE, //
				SET_COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT",//
				SET_COLUMN_MATCH_ID + " INTEGER REFERENCES " + MATCH_TABLE
						+ "(" + MATCH_COLUMN_ID + ") ON DELETE CASCADE",//
				SET_COLUMN_SCORE + " TEXT", //
				SET_COLUMN_TIME + " INTEGER", //
				SET_COLUMN_WINSTATUS + " INTEGER");
		this.createTable(db, MATCH_TABLE, //
				MATCH_COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT", //
				MATCH_COLUMN_OPPONENTID + " INTEGER", //
				MATCH_COLUMN_PLAYERID + " INTEGER", //
				MATCH_COLUMN_SCORE + " TEXT", //
				MATCH_COLUMN_SERVEFIRST + " INTEGER", //
				MATCH_COLUMN_TIME + " INTEGER", //
				MATCH_COLUMN_TRACKINGSTATUS + " TEXT", //
				MATCH_COLUMN_WINSTATUS + " TEXT", //
				MATCH_COLUMN_SURFACE + " TEXT", //
				MATCH_COLUMN_NOTE + " TEXT");
		this.createTable(db, PLAYER_TABLE, //
				PLAYER_COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT", //
				PLAYER_COLUMN_NAME + " TEXT");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO can't just delete old data. think about migration strategy or
		// other solution.
		//db.execSQL("DROP TABLE IF EXISTS " + MATCH_TABLE);
		// db.execSQL("DROP TABLE IF EXISTS " + SET_TABLE);
		// db.execSQL("DROP TABLE IF EXISTS " + GAME_TABLE);
		// db.execSQL("DROP TABLE IF EXISTS " + POINT_TABLE);
		// db.execSQL("DROP TABLE IF EXISTS " + PLAYER_TABLE);
		db.delete(DB_NAME, null, null);
		onCreate(db);
	}

	private void createTable(SQLiteDatabase db, String tableName,
			String... columns) {
		String command = "CREATE TABLE " + tableName + "(";
		for (int i = 0; i < columns.length; i++) {
			command += columns[i];
			if (i + 1 < columns.length) {
				command += ",";
			}
		}
		command += ")";
		db.execSQL(command);
	}

}
