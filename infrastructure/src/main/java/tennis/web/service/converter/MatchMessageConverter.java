/*******************************************************************************
 * Copyright (c) 2012 Amir Kadić.
 * 
 * This file is part of Center Court.
 * 
 * Center Court is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Center Court is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Center Court.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Contributors:
 *     Amir Kadić - initial API and implementation
 ******************************************************************************/
package tennis.web.service.converter;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.springframework.http.HttpInputMessage;
import org.springframework.http.HttpOutputMessage;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.HttpMessageNotWritableException;

import tennis.domain.entity.Match;
import tennis.web.serialization.XmlMatchSerializer;

public class MatchMessageConverter implements HttpMessageConverter<Match> {

	private static HttpMessageConverter<Match> singleton;
	private XmlMatchSerializer serializer;

	public synchronized static HttpMessageConverter<Match> getConverter() {
		if (MatchMessageConverter.singleton == null) {
			MatchMessageConverter.singleton = new MatchMessageConverter();
		}
		return MatchMessageConverter.singleton;
	}

	public MatchMessageConverter() {
		this.serializer = new XmlMatchSerializer();
	}

	public boolean canRead(Class<?> klass, MediaType mediaType) {
		return false;
	}

	public boolean canWrite(Class<?> klass, MediaType mediaType) {
		return klass.equals(Match.class);
	}

	public List<MediaType> getSupportedMediaTypes() {
		return Arrays.asList(MediaType.TEXT_XML);
	}

	public Match read(Class<? extends Match> matchClass,
			HttpInputMessage message) throws IOException,
			HttpMessageNotReadableException {
		return null;
	}

	public void write(Match match, MediaType mediaType,
			HttpOutputMessage message) throws IOException,
			HttpMessageNotWritableException {
		this.serializer.serialize(match, message.getBody());
	}

}
