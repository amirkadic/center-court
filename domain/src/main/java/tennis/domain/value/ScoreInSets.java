/*******************************************************************************
 * Copyright (c) 2012 Amir Kadić.
 * 
 * This file is part of Center Court.
 * 
 * Center Court is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Center Court is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Center Court.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Contributors:
 *     Amir Kadić - initial API and implementation
 ******************************************************************************/
package tennis.domain.value;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import tennis.domain.IllegalScoreException;
import tennis.domain.entity.CountableScore;

public abstract class ScoreInSets {

	public static final ScoreInSets INITIAL_SCORE = new ScoreInSets(
			Arrays.asList(CountableScore.ZERO)) {

		@Override
		public void validate() throws IllegalScoreException {
		}

		@Override
		public boolean canFollow(ScoreInSets pastScore)
				throws IllegalScoreException {
			return false;
		}
	};
	protected List<CountableScore> setScores;

	public ScoreInSets(List<CountableScore> setScores) {
		this.setScores = setScores;
	}

	public int size() {
		return this.setScores.size();
	}

	public boolean equalInSet(int setIndex, ScoreInSets other) {
		return this.setScores.size() > setIndex
				&& other.size() > setIndex
				&& this.setScores.get(setIndex).equals(
						other.setScores.get(setIndex));
	}

	/**
	 * Answers the question: can this score preceed another one? This is used to
	 * decide if a jump forward is possible.
	 * 
	 * @param pastScore
	 *            A valid candidate past score.
	 * @return true if this score can follow pastScore, false otherwise false.
	 * @throws IllegalScoreException
	 *             If this score or the other one is invalid in itself.
	 */
	public abstract boolean canFollow(ScoreInSets pastScore)
			throws IllegalScoreException;

	public abstract void validate() throws IllegalScoreException;

	public CountableScore lastPart() {
		return this.setScores.get(this.setScores.size() - 1);
	}

	public boolean isEmpty() {
		return this.setScores.isEmpty();
	}

	public CountableScore part(int index) {
		return new CountableScore(this.setScores.get(index));
	}

	/**
	 * Partial score.
	 * 
	 * @param offset
	 *            The zero-based index of the sublist.
	 * @return A deep copy of all scores starting at offset to the end of the
	 *         list.
	 */
	public List<CountableScore> partialScore(int offset) {
		List<CountableScore> partialScore = new ArrayList<CountableScore>();
		List<CountableScore> partialList = this.setScores.subList(offset,
				this.setScores.size());
		for (int i = 0; i < partialList.size(); i++) {
			partialScore.add(new CountableScore(partialList.get(i)));
		}
		return partialScore;
	}

	public Iterator<CountableScore> iterator() {
		return this.setScores.iterator();
	}

	@Override
	public boolean equals(Object obj) {
		if (!obj.getClass().equals(this.getClass())) {
			return false;
		}
		return this.setScores.equals(((ScoreInSets) obj).setScores);
	}

	@Override
	public int hashCode() {
		return this.setScores.hashCode();
	}

	@Override
	public String toString() {
		Iterator<CountableScore> setScoreIterator = this.setScores.iterator();
		StringBuffer buffer = new StringBuffer(setScoreIterator.next()
				.toString());
		while (setScoreIterator.hasNext())
			buffer.append(" ").append(setScoreIterator.next().toString());
		return buffer.toString();
	}

}
