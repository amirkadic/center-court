/*******************************************************************************
 * Copyright (c) 2012 Amir Kadić.
 * 
 * This file is part of Center Court.
 * 
 * Center Court is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Center Court is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Center Court.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Contributors:
 *     Amir Kadić - initial API and implementation
 ******************************************************************************/
package tennis.app.gui.activity;

import tennis.domain.service.TrackingService;
import tennis.storage.MatchStore;
import tennis.storage.StorageException;
import android.os.AsyncTask;
import android.util.Log;

public class CurrentMatchPauser extends AsyncTask<TrackingService, Void, Void> {

	private MatchStore matchStore;

	public CurrentMatchPauser(MatchStore matchStore) {
		super();
		this.matchStore = matchStore;
	}

	@Override
	protected Void doInBackground(TrackingService... argList) {
		TrackingService service = argList[0];
		if (service.isRunning()) {
			try {
				this.matchStore.put(service.getMatch());
				service.stop();
			} catch (StorageException e) {
				Log.d("CurrentMatchPauser",
						"Couldn't save match: " + e.getMessage());
			}
		}
		return null;
	}

}
