/*******************************************************************************
 * Copyright (c) 2012 Amir Kadić.
 * 
 * This file is part of Center Court.
 * 
 * Center Court is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Center Court is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Center Court.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Contributors:
 *     Amir Kadić - initial API and implementation
 ******************************************************************************/
package tennis.app.gui.activity;

import java.util.Arrays;

import tennis.app.gui.MatchScoreListener;
import tennis.app.gui.dialog.TargetScoreDialog;
import tennis.app.gui.dto.ParcelableMatchReference;
import tennis.db.SqliteMatchStore;
import tennis.domain.Handedness;
import tennis.domain.IllegalMoveException;
import tennis.domain.IllegalScoreException;
import tennis.domain.Surface;
import tennis.domain.WinStatus;
import tennis.domain.entity.Match;
import tennis.domain.entity.Player;
import tennis.domain.service.DefaultTrackingService;
import tennis.domain.service.TrackingService;
import tennis.domain.value.ScoreInSets;
import tennis.storage.MatchStore;
import tennis.storage.StorageException;
import tennis.app.R;
import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

/**
 * TODO Hide match store behind TrackingService and for every place it is used
 * here, behind a more minimal interface. The MatchStore is exposing more than
 * its current clients need.
 * 
 * @author amir
 * 
 */
public class Tracking extends Activity implements OnClickListener {

	private static final int COMMENT_DIALOG = 0;
	private static final int COMMENT_DIALOG_POINT_WON = 1;
	private static final int COMMENT_DIALOG_POINT_LOST = 2;
	private static final int COMMENT_DIALOG_GAME_WON = 3;
	private static final int COMMENT_DIALOG_GAME_LOST = 4;
	private static final int ABBREVIATIONS_DIALOG = 5;
	private static final int JUMP_DIALOG = 6;

	private TrackingService service;
	private MatchStore matchStore;
	private MatchViewUpdater matchViewUpdater;
	private TargetScoreDialog targetScoreDialog;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		logD("onCreate()");
		setContentView(R.layout.main);
		Button winButton = ((Button) findViewById(R.id.winButton));
		winButton.setOnClickListener(this);
		Button loseButton = ((Button) findViewById(R.id.loseButton));
		loseButton.setOnClickListener(this);
		Button undoButton = ((Button) findViewById(R.id.undoButton));
		undoButton.setOnClickListener(this);
		Button jumpButton = ((Button) findViewById(R.id.jumpButton));
		jumpButton.setOnClickListener(this);
		// TODO optimize (these are singletons)
		// service = DefaultTrackingService.getTrackingService();
		service = DefaultTrackingService.getTrackingService();
		//
		this.matchStore = SqliteMatchStore.getSingleton(this
				.getApplicationContext());

		this.matchViewUpdater = new DefaultMatchViewUpdater(winButton,
				loseButton, (TextView) findViewById(R.id.playerNamesTextView),
				(TextView) findViewById(R.id.setScoresTextView),
				(TextView) findViewById(R.id.gameScoreTextView),
				(Button) findViewById(R.id.undoButton),
				(Button) findViewById(R.id.jumpButton));

		long matchId = getIntent().getExtras().getLong(Constants.MATCH_ID);
		try {
			// TODO loading match is a long-running operation: do in background
			Match match = this.matchStore.getById(matchId);
			if (match != null) {
				match.readyForTracking();
				this.matchViewUpdater.update(match);
				this.service.resume(match);
			} else {
				Log.e("Tracking.onCreate()", "No match found with ID "
						+ matchId + ".");
			}
		} catch (StorageException e) {
			Log.e("Tracking.onCreate()", "Couldn't retrieve match " + matchId
					+ ".", e);
		}

	}

	@Override
	protected void onPause() {
		if (this.service.isRunning()) {
			try {
				this.matchStore.put(this.service.getMatch());
			} catch (StorageException e) {
				handleError("Couldn't save match onPause().", e);
			}
		}
		super.onPause();
	}

	@Override
	protected void onResume() {
		logD("onResume()");
		super.onResume();
		if (this.service.isRunning()) {
			this.matchViewUpdater.update(this.service.getMatch());
		} else {
			this.matchViewUpdater.clear();
		}

		// TODO Figure out when to reload latest match - onResume is a bad idea
		// because it might
		// resume a match that's just about to be deleted. Alternatively, figure
		// out a fast, on-UI-thread deletion.
		// try {
		// currentMatch = this.matchStore.getLatest();
		// service.resume(currentMatch);
		// } catch (Exception e) {
		// this.logException("onResume() failed loading latest match", e);
		// }
	}

	public void onClick(View v) {

		switch (v.getId()) {
		case R.id.winButton:
			Log.d("Tracking", "Won point");
			try {
				this.service.winPoint();
				this.matchViewUpdater.update(this.service.getMatch());
			} catch (IllegalMoveException e) {
				this.longToast("Illegal move! Please discontinue tracking and report this problem");
				this.handleError("Illegal move", e);
				return;
			}
			break;
		case R.id.loseButton:
			Log.d("Tracking", "Lost point");
			try {
				this.service.losePoint();
				this.matchViewUpdater.update(this.service.getMatch());
			} catch (IllegalMoveException e) {
				this.longToast("Illegal move! Please discontinue tracking and report this problem");
				this.handleError("Illegal move", e);
				return;
			}
			break;
		case R.id.undoButton:
			if (this.service.canUndo()) {
				try {
					this.service.undo();
				} catch (IllegalMoveException e) {
					Log.e("Undoing", e.getMessage());
					break;
				}
				Log.d("Tracking", "Undid");
				this.matchViewUpdater.update(this.service.getMatch());
			} else {
				Log.d("Tracking", "Can't undo.");
			}
			break;
		case R.id.jumpButton:
			showDialog(JUMP_DIALOG);
			break;
		default:
			Log.d("Tracking", "Unknown view clicked (ID:" + v.getId() + ").");
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.match, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.comment:
			showDialog(COMMENT_DIALOG);
			return true;
		case R.id.pause:
			// new CurrentMatchPauser(this.matchStore).execute(this.service);
			// this.matchViewUpdater.clear();
			showAllMatches();
			return true;
		case R.id.finish:
			if (this.service.isRunning()) {
				this.service.finishTracking();
			}
			showAllMatches();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case COMMENT_DIALOG:
			return this.createCommentDialog("Comment", "");
		case COMMENT_DIALOG_POINT_WON:
			return this.createCommentDialog("Losing streak broken",
					"Won a point after losing 3. How come?");
		case COMMENT_DIALOG_POINT_LOST:
			return this.createCommentDialog("Winning streak broken",
					"Lost a point after winning 3. How come?");
		case COMMENT_DIALOG_GAME_WON:
			return this.createCommentDialog("Losing streak broken",
					"Won a game after losing 3. How come?");
		case COMMENT_DIALOG_GAME_LOST:
			return this.createCommentDialog("Winning streak broken",
					"Lost a game after winning 3. How come?");
			// case ABBREVIATIONS_DIALOG:
			// return new AlertDialog.Builder( this )
			// .setTitle( "Abbreviations" )
			// .setMultiChoiceItems( _options, _selections, new
			// DialogSelectionClickHandler() )
			// .setPositiveButton( "OK", new DialogButtonClickHandler() )
			// .create();
		case JUMP_DIALOG:
			return this.createJumpDialog();
		}
		return null;
	}

	@Override
	protected void onPrepareDialog(int id, Dialog dialog) {
		if (Arrays.asList(COMMENT_DIALOG, COMMENT_DIALOG_POINT_WON,
				COMMENT_DIALOG_POINT_LOST, COMMENT_DIALOG_GAME_WON,
				COMMENT_DIALOG_GAME_LOST).contains(id)) {

			EditText commentEditText = (EditText) dialog
					.findViewById(R.id.commentfield);
			commentEditText.clearComposingText();
		} else if (id == JUMP_DIALOG) {
			this.targetScoreDialog.prepare(this.service.currentScoreInSets());
		}
	}

	private void showAllMatches() {
		this.shortToast("Loading matches");
		if (this.service.isRunning()) {
			try {
				this.matchStore.put(this.service.getMatch());
			} catch (StorageException e) {
				Log.e("Tracking",
						"Couldn't save match prior to showing list of matches.",
						e);
				longToast("Couldn't save current match. Some tracking data might get lost.");
			}
			this.service.pause();
		}
		Intent allMatches = new Intent(getApplicationContext(),
				AllMatches.class);
		startActivity(allMatches);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch (requestCode) {
		case Constants.REQUEST_INITIAL_DATA:
			if (resultCode == Activity.RESULT_OK) {
				this.service.start();
				Match match = this.service.getMatch();
				Player player = match.getPlayer();
				Player opponent = match.getOpponent();
				Bundle extras = data.getExtras();
				player.setName(extras.getString(Constants.PLAYER_NAME));
				opponent.setName(extras.getString(Constants.OPPONENT_NAME));
				// hard-coded data for upload testing:
				player.setHandedness(Handedness.RIGHT);
				player.setRanking("good player");
				player.setSurface(Surface.GRASS);
				player.setAge(1);
				opponent.setHandedness(Handedness.LEFT);
				opponent.setRanking("also good");
				opponent.setSurface(Surface.GRASS);
				opponent.setAge(2);
				//
				this.matchViewUpdater.update(match);
				new CurrentMatchSaver(this.matchStore).execute(this.service);
			} else {
				// what TODO here?
			}
		case Constants.REQUEST_MATCH_OPERATION:
			this.logD("Received match op.");
			if (resultCode == Constants.RESULT_RESUME_MATCH) {
				ParcelableMatchReference matchReference = data.getExtras()
						.getParcelable(Constants.EXTRA_SELECTED_MATCH);
				this.logD("Resuming match " + matchReference.getId());
				this.shortToast("Loading match");
				new ResumeMatchTask(this.service, this.matchStore,
						this.matchViewUpdater).execute(matchReference.getId());
			}
		default:
			return;
		}
	}

	private void handleError(String message, Exception e) {
		this.logException(message, e);
	}

	private void logException(String message, Exception e) {
		Log.e("Tracking",
				message + ", exception info:" + e + ":" + e.getMessage());
		e.printStackTrace();
	}

	private void logD(String message) {
		Log.d("Tracking", message);
	}

	private void shortToast(String message) {
		Toast toast = Toast.makeText(getApplicationContext(), message,
				Toast.LENGTH_SHORT);
		toast.show();
	}

	private void longToast(String message) {
		Toast toast = Toast.makeText(getApplicationContext(), message,
				Toast.LENGTH_LONG);
		toast.show();
	}

	private Dialog createCommentDialog(String title, String prompt) {
		this.logD("Creating comment dialog.");
		final Dialog dialog = new Dialog(this);
		dialog.setContentView(R.layout.comment);
		dialog.setTitle(title);
		((TextView) dialog.findViewById(R.id.commentPrompt)).setText(prompt);
		dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {

			public void onDismiss(DialogInterface arg0) {
				EditText commentEditText = (EditText) dialog
						.findViewById(R.id.commentfield);
				commentEditText.setText("");
			}
		});

		Button ok = (Button) dialog.findViewById(R.id.commentOKButton);
		ok.setOnClickListener(new OnClickListener() {

			public void onClick(View view) {
				EditText commentEditText = (EditText) dialog
						.findViewById(R.id.commentfield);
				String comment = commentEditText.getText().toString();
				if (service.isRunning()) {
					logD("Comment:" + comment);
					service.commentLastPoint(comment);
				}
				logD("Dismissing comment dialog.");
				dialog.dismiss();
			}
		});
		Button cancelButton = (Button) dialog.findViewById(R.id.cancelButton);
		cancelButton.setOnClickListener(new OnClickListener() {

			public void onClick(View view) {
				logD("Dismissing comment dialog.");
				dialog.dismiss();
			}
		});
		return dialog;

	}

	private Dialog createJumpDialog() {
		this.logD("Creating jump dialog.");
		this.targetScoreDialog = new TargetScoreDialog(this,
				new MatchScoreListener() {

					public void notify(ScoreInSets scoreInSets)
							throws IllegalScoreException {
						jump(scoreInSets);
					}
				});
		return this.targetScoreDialog.getDialog();

	}

	private void jump(ScoreInSets scoreInSets) throws IllegalScoreException {

		if (!scoreInSets.canFollow(service.currentScore().getScoreInSets())) {
			// jumping back is disallowed for now even if implemented
			// by the service
			throw new IllegalScoreException("Invalid target score.");
		}
		if (service.isRunning()) {
			try {
				service.jumpTo(scoreInSets);
				matchViewUpdater.update(service.getMatch());
			} catch (IllegalMoveException e) {
				throw new IllegalScoreException("Invalid target score.");
			}
		}

	}

}
