/*******************************************************************************
 * Copyright (c) 2012 Amir Kadić.
 * 
 * This file is part of Center Court.
 * 
 * Center Court is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Center Court is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Center Court.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Contributors:
 *     Amir Kadić - initial API and implementation
 ******************************************************************************/
package tennis.domain.value;

import tennis.domain.entity.CountableScore;
import tennis.domain.entity.Score;

public class MatchScore {

	public static final MatchScore INITIAL_SCORE = new MatchScore(
			ScoreInSets.INITIAL_SCORE, CountableScore.ZERO);

	private ScoreInSets scoreInSets;
	private Score gameScore;

	public MatchScore(ScoreInSets scoreInSets, Score gameScore) {
		this.scoreInSets = scoreInSets;
		this.gameScore = gameScore;
	}

	public ScoreInSets getScoreInSets() {
		return scoreInSets;
	}

	public Score getGameScore() {
		return gameScore;
	}

	public boolean hasMoreSetsThan(MatchScore other) {
		return this.scoreInSets.size() > other.scoreInSets.size();
	}

	public boolean sameGame(MatchScore other) {
		if (this.scoreInSets.size() == other.scoreInSets.size()) {
			for (int i = 0; i < this.scoreInSets.size(); i++) {
				if (!this.scoreInSets.equalInSet(i, other.scoreInSets)) {
					return false;
				}
			}
			return true;
		}
		return false;
	}


	@Override
	public String toString() {
		if (this.scoreInSets.isEmpty())
			return "";
		return this.scoreInSets + "-" + this.gameScore;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof MatchScore) {
			MatchScore other = (MatchScore) obj;
			return other.scoreInSets.equals(this.scoreInSets)
					&& other.getGameScore().equals(this.gameScore);
		} else {
			return false;
		}

	}

	@Override
	public int hashCode() {
		return this.scoreInSets.hashCode() * this.gameScore.hashCode();
	}

}
