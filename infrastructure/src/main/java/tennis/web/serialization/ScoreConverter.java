/*******************************************************************************
 * Copyright (c) 2012 Amir Kadić.
 * 
 * This file is part of Center Court.
 * 
 * Center Court is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Center Court is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Center Court.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Contributors:
 *     Amir Kadić - initial API and implementation
 ******************************************************************************/
package tennis.web.serialization;

import java.util.HashMap;
import java.util.Map;

import tennis.domain.entity.Advantage;
import tennis.domain.entity.CountableScore;
import tennis.domain.entity.Deuce;
import tennis.domain.entity.Disadvantage;
import tennis.domain.entity.EndScore;
import tennis.domain.entity.Score;

import com.thoughtworks.xstream.converters.basic.AbstractSingleValueConverter;

public class ScoreConverter extends AbstractSingleValueConverter {

	private final Map<String, Score> singletonScores = new HashMap<String, Score>();

	public ScoreConverter() {
		super();
		this.singletonScores.put("deuce", new Deuce());
		this.singletonScores.put("advantage", new Advantage());
		this.singletonScores.put("disadvantage", new Disadvantage());
		this.singletonScores.put("end", new EndScore());
	}

	@Override
	public boolean canConvert(@SuppressWarnings("rawtypes") Class klass) {
		return Score.class.isAssignableFrom(klass);
	}

	@Override
	public Object fromString(String string) {
		if (this.singletonScores.containsKey(string)) {
			return this.singletonScores.get(string);
		} else {
			String[] scoreComponents = string.split(":");
			return new CountableScore(Short.parseShort(scoreComponents[0]),
					Short.parseShort(scoreComponents[1]));
		}
	}

}
