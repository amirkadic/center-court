/*******************************************************************************
 * Copyright (c) 2012 Amir Kadić.
 * 
 * This file is part of Center Court.
 * 
 * Center Court is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Center Court is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Center Court.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Contributors:
 *     Amir Kadić - initial API and implementation
 ******************************************************************************/
package tennis.domain.value;

import java.util.Iterator;
import java.util.List;

import tennis.domain.IllegalScoreException;
import tennis.domain.entity.CountableScore;

public class BestOf3ScoreInSets extends ScoreInSets {

	public BestOf3ScoreInSets(List<CountableScore> setScores) {
		super(setScores);
	}

	public void validate() throws IllegalScoreException {
		if (this.setScores == null || this.setScores.isEmpty()) {
			throw new IllegalScoreException("No score defined.");
		}
		if (this.setScores.size() > 3) {
			throw new IllegalScoreException("Too many sets.");
		}
		// make sure all sets are finished (except maybe the last)
		Iterator<CountableScore> scores = this.setScores.iterator();
		while (scores.hasNext()) {

			CountableScore score = scores.next();
			int won = score.getWon();
			int lost = score.getLost();
			if (won > 7 || lost > 7 || won + lost > 13) {
				throw new IllegalScoreException("Too many games.");
			}
			if (Math.max(won, lost) == 7 && Math.min(won, lost) != 6) {
				throw new IllegalScoreException("Too many games.");
			}
			if (scores.hasNext()) { // all sets but the last have to be finished
				if (!setFinished(won, lost)) {
					throw new IllegalScoreException("Unfinished non-last set.");
				}
			}
		}
		if (this.setsWon() > 2) {
			throw new IllegalScoreException("Too many sets won.");
		}
		if (this.setsLost() > 2) {
			throw new IllegalScoreException("Too many sets lost.");
		}

	}

	public boolean canFollow(ScoreInSets pastScore)
			throws IllegalScoreException {
		this.validate();
		pastScore.validate();
		Iterator<CountableScore> pastSetScores = pastScore.setScores.iterator();
		Iterator<CountableScore> mySetScores = this.setScores.iterator();
		while (pastSetScores.hasNext()) {
			if (!mySetScores.hasNext()) {
				// past score has more set than this one
				return false;
			}
			CountableScore pastSetScore = pastSetScores.next();
			CountableScore mySetScore = mySetScores.next();
			if (!pastSetScore.equals(mySetScore)) {
				// difference found. check if there are any sets after that.
				if (pastSetScores.hasNext()) {
					return false;
				}
				// difference found in last set the past score contains, check
				// if the sequence is
				// possible
				int myWon = mySetScore.getWon();
				int myLost = mySetScore.getLost();
				int pastWon = pastSetScore.getWon();
				int pastLost = pastSetScore.getLost();
				return myWon >= pastWon && myLost >= pastLost
						&& myWon + myLost > pastWon + pastLost;

			}
		}
		// no difference found in the common sets
		return mySetScores.hasNext();
	}

	private boolean setFinished(int won, int lost) {
		return (Math.max(won, lost) == 6 && Math.abs(won - lost) > 1)
				|| (Math.max(won, lost) == 7 && Math.min(won, lost) == 6);
	}

	private int setsWon() {
		int setsWon = 0;
		for (CountableScore setScore : this.setScores) {
			int won = setScore.getWon();
			int lost = setScore.getLost();
			if ((won == 6 && won - lost > 1) || (won == 7 && lost == 6)) {
				setsWon++;
			}
		}
		return setsWon;
	}

	private int setsLost() {
		int setsLost = 0;
		for (CountableScore setScore : this.setScores) {
			int won = setScore.getWon();
			int lost = setScore.getLost();
			if ((lost == 6 && lost - won > 1) || (lost == 7 && won == 6)) {
				setsLost++;
			}
		}
		return setsLost;
	}

}
