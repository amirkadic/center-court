/*******************************************************************************
 * Copyright (c) 2012 Amir Kadić.
 * 
 * This file is part of Center Court.
 * 
 * Center Court is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Center Court is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Center Court.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Contributors:
 *     Amir Kadić - initial API and implementation
 ******************************************************************************/
package tennis.domain.memento;

import java.util.Date;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import tennis.domain.GameFactory;
import tennis.domain.IllegalMoveException;
import tennis.domain.WinStatus;
import tennis.domain.entity.CountableScore;
import tennis.domain.entity.Game;
import tennis.domain.entity.Point;
import tennis.domain.memento.GameMemento;
import tennis.domain.memento.RegularGameMemento;

public class GameMementoTest {
	private static final GameFactory gameFactory = new GameFactory();
	
	@BeforeClass
	public static void setup() {
	}

	@Test
	public void testMemento() throws IllegalMoveException {
		Game game = GameMementoTest.gameFactory.startNewGame();
		game.setServe(true);
		Assert.assertEquals(WinStatus.PENDING, game.getStatus());
		// 15:0
		Point point = new Point();
		point.setStatus(WinStatus.WON);
		Date firstTime = new Date();
		point.setDate(firstTime);
		game.add(point);
		Assert.assertEquals(new CountableScore(15, 0), game.getScore());
		Assert.assertEquals(new CountableScore(15, 0), point.getScore());
		GameMemento memento = game.saveToMemento();
		// 30:0
		point = new Point();
		point.setStatus(WinStatus.WON);
		point.setDate(new Date());
		game.add(point);
		
		// 40:0
		point = new Point();
		point.setStatus(WinStatus.WON);
		point.setDate(new Date());
		game.add(point);
		// game first player
		point = new Point();
		point.setStatus(WinStatus.WON);
		point.setDate(new Date());
		game.add(point);

		Assert.assertTrue(memento instanceof RegularGameMemento);
		RegularGameMemento regularGameMemento = (RegularGameMemento) memento;
		Assert.assertEquals(WinStatus.PENDING, regularGameMemento.getStatus());
		Assert.assertEquals(new CountableScore(15, 0), regularGameMemento.getPlayElement().getScore());
		Assert.assertEquals(true, regularGameMemento.isServe());
		Assert.assertEquals(game.getTime(), regularGameMemento.getTime());
		Assert.assertEquals(WinStatus.PENDING, regularGameMemento.getPlayElement().getWinStatus());

	}

}
