/*******************************************************************************
 * Copyright (c) 2012 Amir Kadić.
 * 
 * This file is part of Center Court.
 * 
 * Center Court is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Center Court is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Center Court.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Contributors:
 *     Amir Kadić - initial API and implementation
 ******************************************************************************/
package tennis.storage;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import tennis.domain.entity.Match;
import tennis.storage.MatchMarshaller;
import tennis.storage.MatchStore;
import tennis.storage.StorageException;
import tennis.storage.XStreamMatchMarshaller;
import android.content.Context;
import android.util.Log;

public class InMemoryMatchStore implements MatchStore {

	private static InMemoryMatchStore store = null;
	private MatchMarshaller marshaller;
	private Context context;

	/**
	 * Singleton access.
	 * 
	 * @param context
	 *            Android content context
	 * @return The local match store.
	 */
	public static synchronized MatchStore getStore(Context context) {
		if (InMemoryMatchStore.store == null) {
			InMemoryMatchStore.store = new InMemoryMatchStore();
			InMemoryMatchStore.store
					.setMarshaller(new XStreamMatchMarshaller());
			if (InMemoryMatchStore.store.context == null) {
				InMemoryMatchStore.store.setContext(context);
			}
		}
		return InMemoryMatchStore.store;
	}

	private InMemoryMatchStore() {
		// no direct instantiation, use factory method
	}

	public synchronized void put(Match match) throws StorageException {
		String matchAsString = this.marshaller.marshal(match);
		try {
			FileOutputStream os = context.openFileOutput(
					"match-" + match.getTime() + ".xml", Context.MODE_PRIVATE);
			os.write(matchAsString.getBytes("UTF-8"));
			os.close();
			this.deleteAnyLatest();
			os = context.openFileOutput("latest" + match.getTime(),
					Context.MODE_PRIVATE);
			// os.write("");
			os.close();
		} catch (FileNotFoundException e) {
			throw new StorageException("File not found.");
		} catch (UnsupportedEncodingException e) {
			throw new StorageException("UTF-8 not supported.");
		} catch (IOException e) {
			throw new StorageException("I/O error: " + e.getMessage());
		}

	}

	public synchronized List<Match> allMatches() throws StorageException {
		List<Match> matches = new ArrayList<Match>();
		for (String fileName : this.allMatchFileNames()) {
			if (fileName.startsWith("match")) {
				matches.add(this.unmarshalMatch(fileName));
			}
		}
		return matches;
	}

	public synchronized void remove(Match match) {
		context.deleteFile("match-" + match.getTime() + ".xml");
		context.deleteFile("latest" + match.getTime());
	}

	public synchronized Match getLatest() throws StorageException {
		String lastSavedTimestamp = this.lastSavedTimestamp();
		if (lastSavedTimestamp.equals("")) {
			return null;
		}
		return this.unmarshalMatch("match-" + lastSavedTimestamp + ".xml");
	}

	public synchronized Match getByDate(long date) throws StorageException {
		return this.unmarshalMatch("match-" + date + ".xml");
	}

	private String lastSavedTimestamp() {
		for (String fileName : context.fileList()) {
			if (fileName.startsWith("latest"))
				return fileName.substring(6);
		}
		return "";
	}

	private void deleteAnyLatest() {
		for (String fileName : context.fileList()) {
			if (fileName.startsWith("latest"))
				context.deleteFile(fileName);
		}
	}

	private Match unmarshalMatch(String fileName) throws StorageException {
		try {
			Log.d("LocalMatchStore", "Trying to unmarshal " + fileName + ".");
			FileInputStream is = context.openFileInput(fileName);
			// FIXME make this dynamic
			ByteArrayOutputStream bos = new ByteArrayOutputStream(1024 * 10);
			int octet = -1;
			while ((octet = is.read()) != -1) {
				bos.write(octet);
			}
			bos.close();
			String matchAsString = bos.toString("UTF-8");
			return this.marshaller.unmarshal(matchAsString);
		} catch (FileNotFoundException e) {
			// throw new StorageException("File not found.");
			// FIXME avoid expection handling by checking if file exists first
			return null;
		} catch (IOException e) {
			throw new StorageException("I/O error: " + e.getMessage());
		}
	}

	private String[] allMatchFileNames() {
		return context.fileList();
	}

	public void setMarshaller(MatchMarshaller marshaller) {
		this.marshaller = marshaller;
	}

	public void setContext(Context context) {
		this.context = context;
	}

	public Match getById(long id) throws StorageException {
		throw new UnsupportedOperationException();
	}

	public boolean remove(long matchId) {
		throw new UnsupportedOperationException();
	}

}
