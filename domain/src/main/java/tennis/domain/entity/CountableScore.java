/*******************************************************************************
 * Copyright (c) 2012 Amir Kadić.
 * 
 * This file is part of Center Court.
 * 
 * Center Court is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Center Court is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Center Court.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Contributors:
 *     Amir Kadić - initial API and implementation
 ******************************************************************************/
package tennis.domain.entity;

import tennis.domain.WinStatus;

public class CountableScore extends Score {

	public static final CountableScore ZERO = new CountableScore(0, 0);

	private short won;
	private short lost;

	public CountableScore() {
		// for XStream compatibility only
	}

	public CountableScore(short won, short lost) {
		this.won = won;
		this.lost = lost;
	}

	/**
	 * Convenience constructor so factories don't have to do the casting.
	 * 
	 * @param won
	 *            # of units won
	 * @param lost
	 *            # of units lost
	 */
	public CountableScore(int won, int lost) {
		this.won = (short) won;
		this.lost = (short) lost;
	}

	/**
	 * Copy constructor.
	 * 
	 * @param other
	 *            The score to copy values from.
	 */
	public CountableScore(CountableScore other) {
		this.won = other.won;
		this.lost = other.lost;
	}

	public short getWon() {
		return won;
	}

	public short getLost() {
		return lost;
	}

	public CountableScore count(WinStatus status) {
		if (status == WinStatus.WON) {
			return new CountableScore((short) (this.won + 1), this.lost);
		} else if (status == WinStatus.LOST) {
			return new CountableScore(this.won, (short) (this.lost + 1));

		} else {
			return this;
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof CountableScore) {
			CountableScore other = (CountableScore) obj;
			return this.won == other.won && this.lost == other.lost;
		} else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return new Integer((this.won - this.lost + 1)
				* ((this.won * this.lost) + 1)).hashCode();
	}

	@Override
	public String toString() {
		return this.won + ":" + this.lost;
	}

	@Override
	public boolean canFollow(Score pastScore) {
		// a countable score can never follow deuce, (dis)advantage, or end
		// score
		if (!(pastScore instanceof CountableScore)) {
			return false;
		}
		CountableScore pastCountableScore = (CountableScore) pastScore;
		return !pastCountableScore.equals(this)
				&& (!(this.won < pastCountableScore.won || this.lost < pastCountableScore.lost));
	}
}
