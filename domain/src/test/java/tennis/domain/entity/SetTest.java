/*******************************************************************************
 * Copyright (c) 2012 Amir Kadić.
 * 
 * This file is part of Center Court.
 * 
 * Center Court is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Center Court is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Center Court.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Contributors:
 *     Amir Kadić - initial API and implementation
 ******************************************************************************/
package tennis.domain.entity;

import java.util.Date;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import tennis.domain.GameFactory;
import tennis.domain.IllegalMoveException;
import tennis.domain.SetFactory;
import tennis.domain.WinStatus;
import tennis.domain.entity.CountableScore;
import tennis.domain.entity.Game;
import tennis.domain.entity.Point;
import tennis.domain.entity.Set;
import tennis.domain.entity.Tiebreak;
import tennis.domain.memento.SetMemento;

public class SetTest {

	private static final SetFactory setFactory = new SetFactory();

	@BeforeClass
	public static void setup() {
		GameFactory gameFactory = new GameFactory();
		SetTest.setFactory.setGameFactory(gameFactory);
	}

	@Test
	public void straightWin() throws IllegalMoveException {
		Set set = SetTest.setFactory.startNewSet();

		for (int i = 0; i < 24; i++) {
			Point winner = new Point();
			winner.setStatus(WinStatus.WON);
			winner.setDate(new Date());
			set.add(winner);
		}
		Assert.assertEquals(WinStatus.WON, set.getStatus());
		Assert.assertEquals(new CountableScore(6, 0), set.getCurrentScore());
	}

	@Test
	public void straightLoss() throws IllegalMoveException {
		Set set = SetTest.setFactory.startNewSet();

		for (int i = 0; i < 24; i++) {
			Point winner = new Point();
			winner.setStatus(WinStatus.LOST);
			winner.setDate(new Date());
			set.add(winner);
		}
		Assert.assertEquals(WinStatus.LOST, set.getStatus());
		Assert.assertEquals(new CountableScore(0, 6), set.getCurrentScore());
	}

	@Test(expected = IllegalMoveException.class)
	public void illegalPoint() throws IllegalMoveException {
		Set set = SetTest.setFactory.startNewSet();

		for (int i = 0; i < 24; i++) {
			Point winner = new Point();
			winner.setStatus(WinStatus.WON);
			winner.setDate(new Date());
			set.add(winner);
		}
		Assert.assertEquals(WinStatus.WON, set.getStatus());
		Assert.assertEquals(new CountableScore(6, 0), set.getCurrentScore());

		Point illegalPoint = new Point();
		illegalPoint.setStatus(WinStatus.WON);
		illegalPoint.setDate(new Date());
		set.add(illegalPoint);

	}

	@Test
	public void memento() throws IllegalMoveException {
		Set set = SetTest.setFactory.startNewSet();
		SetMemento initialMemento = set.saveToMemento();
		// first, a straight set loss
		for (int i = 0; i < 24; i++) {
			Point winner = new Point();
			winner.setStatus(WinStatus.LOST);
			winner.setDate(new Date());
			set.add(winner);
		}
		Assert.assertEquals(WinStatus.LOST, set.getStatus());
		Assert.assertEquals(new CountableScore(0, 6), set.getCurrentScore());
		SetMemento finishedMemento = set.saveToMemento();
		// restore to clean set
		set = Set.restore(initialMemento);
		// lose 3 games again
		for (int i = 0; i < 12; i++) {
			Point winner = new Point();
			winner.setStatus(WinStatus.LOST);
			winner.setDate(new Date());
			set.add(winner);
		}
		Assert.assertEquals(WinStatus.PENDING, set.getStatus());
		Assert.assertEquals(new CountableScore(0, 3), set.getCurrentScore());
		// save in the middle of set
		SetMemento loveGamesToThreeMemento = set.saveToMemento();
		// go on to lose the set again
		for (int i = 0; i < 12; i++) {
			Point winner = new Point();
			winner.setStatus(WinStatus.LOST);
			winner.setDate(new Date());
			set.add(winner);
		}
		Assert.assertEquals(WinStatus.LOST, set.getStatus());
		Assert.assertEquals(new CountableScore(0, 6), set.getCurrentScore());
		// go back to mid-set
		set = Set.restore(loveGamesToThreeMemento);
		Assert.assertEquals(WinStatus.PENDING, set.getStatus());
		Assert.assertEquals(new CountableScore(0, 3), set.getCurrentScore());
		// lose a game, leading to 0:4
		for (int i = 0; i < 4; i++) {
			Point winner = new Point();
			winner.setStatus(WinStatus.LOST);
			winner.setDate(new Date());
			try {
				set.add(winner);
			} catch (IllegalMoveException e) {
				throw new RuntimeException("Illegal move at i=" + i, e);
			}
		}
		Assert.assertEquals(WinStatus.PENDING, set.getStatus());
		Assert.assertEquals(new CountableScore(0, 4), set.getCurrentScore());
		// lose 2 games, leading to 0:6 again
		for (int i = 0; i < 8; i++) {
			Point winner = new Point();
			winner.setStatus(WinStatus.LOST);
			winner.setDate(new Date());
			set.add(winner);
		}
		Assert.assertEquals(WinStatus.LOST, set.getStatus());
		Assert.assertEquals(new CountableScore(0, 6), set.getCurrentScore());

	}

	@Test
	public void fastForward() throws IllegalMoveException {
		Set set = SetTest.setFactory.startNewSet();

		for (int i = 0; i < 2; i++) {
			Point winner = new Point();
			winner.setStatus(WinStatus.WON);
			winner.setDate(new Date());
			set.add(winner);
		}
		Assert.assertEquals(WinStatus.PENDING, set.getStatus());
		Assert.assertEquals(new CountableScore(0, 0), set.getCurrentScore());
		set.fastForwardTo(new CountableScore(5, 0));
		Assert.assertEquals(WinStatus.PENDING, set.getStatus());
		Assert.assertEquals(new CountableScore(5, 0), set.getCurrentScore());
		Assert.assertEquals(new CountableScore(0, 0), set.getCurrentGame()
				.getScore());
		for (int i = 0; i < 4; i++) {
			Point winner = new Point();
			winner.setStatus(WinStatus.WON);
			winner.setDate(new Date());
			set.add(winner);
		}
		Assert.assertEquals(WinStatus.WON, set.getStatus());
		Assert.assertEquals(new CountableScore(6, 0), set.getCurrentScore());

	}

	@Test
	public void tiebreakWin() throws IllegalMoveException {
		Set set = SetTest.setFactory.startNewSet();

		for (int i = 0; i < 20; i++) {
			Point winner = new Point();
			winner.setStatus(WinStatus.WON);
			winner.setDate(new Date());
			set.add(winner);
		}
		for (int i = 0; i < 24; i++) {
			Point winner = new Point();
			winner.setStatus(WinStatus.LOST);
			winner.setDate(new Date());
			set.add(winner);
		}
		for (int i = 0; i < 4; i++) {
			Point winner = new Point();
			winner.setStatus(WinStatus.WON);
			winner.setDate(new Date());
			set.add(winner);
		}
		Assert.assertEquals(WinStatus.PENDING, set.getStatus());
		Assert.assertEquals(new CountableScore(6, 6), set.getCurrentScore());

		for (int i = 0; i < 1; i++) {
			Point winner = new Point();
			winner.setStatus(WinStatus.WON);
			winner.setDate(new Date());
			set.add(winner);
			Game currentGame = set.getCurrentGame();
			Assert.assertTrue(currentGame instanceof Tiebreak);
		}
		// win tiebreak
		for (int i = 0; i < 5; i++) {
			Point winner = new Point();
			winner.setStatus(WinStatus.WON);
			winner.setDate(new Date());
			set.add(winner);
		}
		for (int i = 0; i < 1; i++) {
			Point winner = new Point();
			winner.setStatus(WinStatus.WON);
			winner.setDate(new Date());
			set.add(winner);
		}
		Assert.assertEquals(WinStatus.WON, set.getStatus());
		Assert.assertEquals(new CountableScore(7, 6), set.getCurrentScore());

	}

}
