/*******************************************************************************
 * Copyright (c) 2012 Amir Kadić.
 * 
 * This file is part of Center Court.
 * 
 * Center Court is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Center Court is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Center Court.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Contributors:
 *     Amir Kadić - initial API and implementation
 ******************************************************************************/
package tennis.app.gui.dialog;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import tennis.app.gui.MatchScoreListener;
import tennis.app.gui.widget.NumberPicker;
import tennis.domain.IllegalScoreException;
import tennis.domain.entity.CountableScore;
import tennis.domain.entity.Score;
import tennis.domain.value.BestOf3ScoreInSets;
import tennis.domain.value.MatchScore;
import tennis.domain.value.ScoreInSets;
import tennis.app.R;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.Toast;

public class TargetScoreDialog {

	private static final List<PickerPair> pickerPairs = Arrays.asList(
			new PickerPair(R.id.set1PlayerPicker, R.id.set1OpponentPicker),
			new PickerPair(R.id.set2PlayerPicker, R.id.set2OpponentPicker),
			new PickerPair(R.id.set3PlayerPicker, R.id.set3OpponentPicker));
	private Dialog dialog;
	private ScoreInSets initialScoreInSets;

	private static final Map<Integer, int[]> enabledViewsForNumberOfSets = new HashMap<Integer, int[]>() {
		/**
		 * A mapping between the number of sets in the initial score to those
		 * Views in the dialog that will be enabled for that number of sets.
		 */
		private static final long serialVersionUID = 1L;

		{
			put(1, new int[] { R.id.targetScoreSecondSetEnableCheckBox,
					R.id.targetScoreThirdSetEnableCheckBox,
					R.id.set1PlayerPicker, R.id.set1OpponentPicker });
			put(2, new int[] { R.id.targetScoreThirdSetEnableCheckBox,
					R.id.set2PlayerPicker, R.id.set2OpponentPicker });
			put(3, new int[] { R.id.set3PlayerPicker, R.id.set3OpponentPicker });
		}
	};
	private static final Map<Integer, int[]> disabledViewsForNumberOfSets = new HashMap<Integer, int[]>() {
		/**
		 * A mapping between the number of sets in the initial score to those
		 * Views in the dialog that will be disabled for that number of sets.
		 */
		private static final long serialVersionUID = 1L;

		{
			put(1, new int[] { R.id.set2PlayerPicker, R.id.set2OpponentPicker,
					R.id.set3PlayerPicker, R.id.set3OpponentPicker });
			put(2, new int[] { R.id.targetScoreSecondSetEnableCheckBox,
					R.id.set1PlayerPicker, R.id.set1OpponentPicker,
					R.id.set3PlayerPicker, R.id.set3OpponentPicker });
			put(3, new int[] { R.id.targetScoreSecondSetEnableCheckBox,
					R.id.targetScoreThirdSetEnableCheckBox,
					R.id.set1PlayerPicker, R.id.set1OpponentPicker,
					R.id.set2PlayerPicker, R.id.set2OpponentPicker });
		}
	};

	public TargetScoreDialog(Context context,
			final MatchScoreListener matchScoreListener) {
		this.dialog = new Dialog(context);
		Log.d("NewMatchData", "Creating initial score dialog.");
		dialog.setContentView(R.layout.targetscore);
		dialog.setTitle("Target score");
		dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {

			public void onDismiss(DialogInterface dif) {
				checkCheckBoxes(false, dialog,
						R.id.targetScoreSecondSetEnableCheckBox,
						R.id.targetScoreThirdSetEnableCheckBox);
				resetPickers(dialog, R.id.set1PlayerPicker,
						R.id.set1OpponentPicker, R.id.set2PlayerPicker,
						R.id.set2OpponentPicker, R.id.set3PlayerPicker,
						R.id.set3OpponentPicker);
			}
		});

		setupScoreListener(matchScoreListener);
		Button cancelJump = (Button) dialog.findViewById(R.id.cancelButton);
		cancelJump.setOnClickListener(new View.OnClickListener() {

			public void onClick(View view) {
				dialog.dismiss();
			}
		});

		setEnabledViews(true, dialog, R.id.set1PlayerPicker,
				R.id.set1OpponentPicker);
		setEnabledViews(false, dialog, R.id.set2PlayerPicker,
				R.id.set2OpponentPicker);
		((CheckBox) dialog
				.findViewById(R.id.targetScoreSecondSetEnableCheckBox))
				.setOnCheckedChangeListener(new OnCheckedChangeListener() {

					public void onCheckedChanged(CompoundButton cb,
							boolean checked) {
						setEnabledViews(checked, dialog, R.id.set2PlayerPicker,
								R.id.set2OpponentPicker);
						if (!checked) {
							checkCheckBoxes(false, dialog,
									R.id.targetScoreThirdSetEnableCheckBox);
						}
					}
				});
		dialog.findViewById(R.id.set3PlayerPicker).setEnabled(false);
		dialog.findViewById(R.id.set3OpponentPicker).setEnabled(false);
		((CheckBox) dialog.findViewById(R.id.targetScoreThirdSetEnableCheckBox))
				.setOnCheckedChangeListener(new OnCheckedChangeListener() {

					public void onCheckedChanged(CompoundButton cb,
							boolean checked) {
						setEnabledViews(checked, dialog, R.id.set3PlayerPicker,
								R.id.set3OpponentPicker);
						if (checked) {
							checkCheckBoxes(true, dialog,
									R.id.targetScoreSecondSetEnableCheckBox);
						}
					}
				});

	}


	public void prepare(ScoreInSets initialScore) {
		if (initialScore == null) {
			initialScore = BestOf3ScoreInSets.INITIAL_SCORE;
		}
		initializePickers(initialScore);
		initializeCheckBoxes(initialScore);
		setEnabledViews(true, dialog,
				enabledViewsForNumberOfSets.get(initialScore.size()));
		setEnabledViews(false, dialog,
				disabledViewsForNumberOfSets.get(initialScore.size()));
		this.initialScoreInSets = initialScore;
	}

	private void initializeCheckBoxes(ScoreInSets score) {
		int numberOfSets = score.size();
		if (numberOfSets > 1) {
			((CheckBox) dialog
					.findViewById(R.id.targetScoreSecondSetEnableCheckBox))
					.setChecked(true);
		}
		if (numberOfSets > 2) {
			((CheckBox) dialog
					.findViewById(R.id.targetScoreThirdSetEnableCheckBox))
					.setChecked(true);
		}
	}

	public Dialog getDialog() {
		return dialog;
	}

	private void setupScoreListener(final MatchScoreListener matchScoreListener) {
		Button doJump = (Button) dialog.findViewById(R.id.setScoreButton);
		doJump.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {
				try {
					ScoreInSets targetScore = targetScore();
					if (!targetScore.equals(initialScoreInSets)) {
						matchScoreListener.notify(targetScore);
					}
					dialog.dismiss();
				} catch (IllegalScoreException e) {
					Toast toast = Toast.makeText(dialog.getContext(),
							e.getMessage(), Toast.LENGTH_SHORT);
					toast.show();
				}
			}
		});
	}

	private void initializePickers(ScoreInSets score) {
		Log.d("TargetScoreDialog", "Initializing pickers to score '" + score
				+ "'.");
		Iterator<PickerPair> pickerPairIterator = pickerPairs.iterator();
		Iterator<CountableScore> setScoreIterator = score.iterator();
		while (setScoreIterator.hasNext() && pickerPairIterator.hasNext()) {
			this.setPickerValues(setScoreIterator.next(),
					pickerPairIterator.next());
		}
	}

	private void setEnabledViews(boolean enabled, Dialog dialog, int... viewIds) {
		for (int id : viewIds) {
			dialog.findViewById(id).setEnabled(enabled);
		}
	}

	private void checkCheckBoxes(boolean checked, Dialog dialog, int... viewIds) {
		for (int id : viewIds) {
			CheckBox checkBox = (CheckBox) dialog.findViewById(id);
			checkBox.setChecked(checked);
		}
	}

	private void addScore(List<CountableScore> scores, CompoundButton cb,
			Dialog dialog, int playerScorePickerId, int opponentScorePickerId) {
		if (cb.isChecked()) {
			scores.add(pickerValue(dialog, playerScorePickerId,
					opponentScorePickerId));
		}
	}

	private CountableScore pickerValue(Dialog dialog, int playerScorePickerId,
			int opponentScorePickerId) {
		return new CountableScore(
				((NumberPicker) dialog.findViewById(playerScorePickerId))
						.getValue(),
				((NumberPicker) dialog.findViewById(opponentScorePickerId))
						.getValue());
	}

	private void resetPickers(Dialog dialog, int... pickerIds) {
		for (int id : pickerIds) {
			((NumberPicker) dialog.findViewById(id)).setValue(0);
		}
	}

	private void setPickerValues(CountableScore score, PickerPair pickerPair) {
		((NumberPicker) dialog.findViewById(pickerPair.getPlayerPickerId()))
				.setValue(score.getWon());
		((NumberPicker) dialog.findViewById(pickerPair.getOpponentPickerId()))
				.setValue(score.getLost());
	}

	private ScoreInSets targetScore() {
		List<CountableScore> setScores = new ArrayList<CountableScore>();
		setScores.add(pickerValue(dialog, R.id.set1PlayerPicker,
				R.id.set1OpponentPicker));
		addScore(setScores,
				(CompoundButton) dialog
						.findViewById(R.id.targetScoreSecondSetEnableCheckBox),
				dialog, R.id.set2PlayerPicker, R.id.set2OpponentPicker);
		addScore(setScores,
				(CompoundButton) dialog
						.findViewById(R.id.targetScoreThirdSetEnableCheckBox),
				dialog, R.id.set3PlayerPicker, R.id.set3OpponentPicker);

		return new BestOf3ScoreInSets(setScores);

	}

	private static class PickerPair {
		private int playerPickerId;
		private int opponentPickerId;

		private PickerPair(int playerPickerId, int opponentPickerId) {
			this.playerPickerId = playerPickerId;
			this.opponentPickerId = opponentPickerId;
		}

		public int getPlayerPickerId() {
			return playerPickerId;
		}

		public int getOpponentPickerId() {
			return opponentPickerId;
		}

	}

}
