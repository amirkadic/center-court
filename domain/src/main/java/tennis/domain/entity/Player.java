/*******************************************************************************
 * Copyright (c) 2012 Amir Kadić.
 * 
 * This file is part of Center Court.
 * 
 * Center Court is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Center Court is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Center Court.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Contributors:
 *     Amir Kadić - initial API and implementation
 ******************************************************************************/
package tennis.domain.entity;

import tennis.domain.Handedness;
import tennis.domain.Surface;

public class Player {

	private Handedness handedness;
	private int age;
	private String name;
	private Surface surface;
	private String ranking;
	private long id;

	public Player() {
		this.handedness = Handedness.UNKNOWN;
		this.age = 0;
		this.name = "";
		this.surface = Surface.OTHER;
		this.ranking = "";
	}

	public void validate() {
		if (name == null || name.length() == 0) {
			throw new IllegalStateException("No name defined.");
		}
	}

	public Handedness getHandedness() {
		return handedness;
	}

	public void setHandedness(Handedness handedness) {
		this.handedness = handedness;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Surface getSurface() {
		return surface;
	}

	public void setSurface(Surface surface) {
		this.surface = surface;
	}

	public String getRanking() {
		return ranking;
	}

	public void setRanking(String ranking) {
		this.ranking = ranking;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return name != null ? name : "";
	}

}
