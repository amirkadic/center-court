/*******************************************************************************
 * Copyright (c) 2012 Amir Kadić.
 * 
 * This file is part of Center Court.
 * 
 * Center Court is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Center Court is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Center Court.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Contributors:
 *     Amir Kadić - initial API and implementation
 ******************************************************************************/
package tennis.domain;

import tennis.domain.entity.RegularGame;
import tennis.domain.entity.Tiebreak;

public class GameFactory {

	private PlayGraphFactory playGraphFactory = new PlayGraphFactory();

	public RegularGame startNewGame() {
		RegularGame game = new RegularGame();
		game.setPlayGraphFactory(this.playGraphFactory);
		game.start();
		return game;
	}
	
	public Tiebreak startTiebreak() {
		Tiebreak tiebreak = new Tiebreak();
		tiebreak.start();
		return tiebreak;
	}

}
