/*******************************************************************************
 * Copyright (c) 2012 Amir Kadić.
 * 
 * This file is part of Center Court.
 * 
 * Center Court is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Center Court is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Center Court.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Contributors:
 *     Amir Kadić - initial API and implementation
 ******************************************************************************/
package tennis.domain.entity;
//TODO can be singletonized for memory efficiency
public class EndScore extends Score {

	@Override
	public String toString() {
		return "end";
	}

	@Override
	public boolean equals(Object obj) {
		return obj instanceof EndScore;
	}

	@Override
	public int hashCode() {
		return "end".hashCode();
	}

	@Override
	public boolean canFollow(Score pastScore) {
		// an end score can follow any score except another end score
		return !(pastScore instanceof EndScore);
	}

}
