/*******************************************************************************
 * Copyright (c) 2012 Amir Kadić.
 * 
 * This file is part of Center Court.
 * 
 * Center Court is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Center Court is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Center Court.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Contributors:
 *     Amir Kadić - initial API and implementation
 ******************************************************************************/
package tennis.app.gui.activity;

import android.app.Activity;

public class Constants {

	public static final int REQUEST_INITIAL_DATA = 0;
	public static final int RESULT_INITIAL_DATA = Activity.RESULT_FIRST_USER;
	public static final String PLAYER_NAME = "player name";
	public static final String OPPONENT_NAME = "opponent name";
	public static final String MATCH_LABELS = "match labels";
	public static final int RESULT_DELETE_MATCH = Activity.RESULT_FIRST_USER + 1;
	public static final int RESULT_RESUME_MATCH = Activity.RESULT_FIRST_USER + 2;
	public static final int REQUEST_MATCH_OPERATION = 1;
	public static final String EXTRA_SELECTED_MATCH = "selected match";
	public static final String MATCH_ID = Constants.class.getCanonicalName() + "#MATCH_ID";

}
