/*******************************************************************************
 * Copyright (c) 2012 Amir Kadić.
 * 
 * This file is part of Center Court.
 * 
 * Center Court is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Center Court is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Center Court.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Contributors:
 *     Amir Kadić - initial API and implementation
 ******************************************************************************/
package tennis.domain.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import tennis.domain.GameFactory;
import tennis.domain.IllegalMoveException;
import tennis.domain.IllegalScoreException;
import tennis.domain.PlayGraphFactory;
import tennis.domain.SetFactory;
import tennis.domain.Surface;
import tennis.domain.WinStatus;
import tennis.domain.memento.MatchMemento;
import tennis.domain.memento.SetMemento;
import tennis.domain.value.BestOf3ScoreInSets;
import tennis.domain.value.MatchScore;
import tennis.domain.value.ScoreInSets;

import com.thoughtworks.xstream.annotations.XStreamImplicit;

public class Match {

	private String xmlns = "http://www.center-court.net/app";
	// state (changes as match progresses)
	private CountableScore currentScore;
	private WinStatus status = WinStatus.PENDING;
	@XStreamImplicit(itemFieldName = "set")
	private List<Set> sets = new ArrayList<Set>();
	private TrackingStatus trackingStatus = TrackingStatus.NOT_STARTED;
	private Surface surface;
	private String note;

	// immutable fields
	private SetFactory setFactory;
	private long time;
	private Date date;
	private boolean serveFirst;
	private Player player;
	private Player opponent;
	private long id;

	public Match() {
	}

	public Match(long id, long time, boolean serveFirst, Player player,
			Player opponent, 
			TrackingStatus trackingStatus, WinStatus winStatus,
			CountableScore currentScore, SetFactory setFactory, List<Set> sets,
			Surface surface, String note) {
		this.id = id;
		this.time = time;
		this.date = new Date(this.time);
		this.serveFirst = serveFirst;
		this.player = player;
		this.opponent = opponent;
		this.trackingStatus = trackingStatus;
		this.status = winStatus;
		this.currentScore = currentScore;
		if (this.trackingStatus != TrackingStatus.NOT_STARTED) {
			this.setFactory = setFactory;
		}
		this.sets = sets;
		this.surface = surface;
		this.note = note;
	}

	public void start() {
		this.time = System.currentTimeMillis();
		this.date = new Date(this.time);
		this.currentScore = new CountableScore((short) 0, (short) 0);
		this.trackingStatus = TrackingStatus.TRACKING;
		GameFactory gameFactory = new GameFactory();
		this.setFactory.setGameFactory(gameFactory);
		// TODO pass time here: set starts perfectly simultaneously (same goes
		// for games)
		this.sets.add(this.setFactory.startNewSet());
	}

	public void add(Point point) throws IllegalMoveException {

		if (this.status != WinStatus.PENDING) {
			throw new IllegalMoveException(
					"Can't add a point to a match that was already "
							+ this.status + ".");
		}

		// start a new set if the current one was decided already
		if (this.currentSet().getStatus() != WinStatus.PENDING) {
			this.sets.add(this.setFactory.startNewSet());
		}

		this.currentSet().add(point);
		this.updateStatus();
	}

	public MatchScore currentScore() {
		List<CountableScore> setScores = new ArrayList<CountableScore>();
		for (Set set : this.getSets()) {
			setScores.add(set.getCurrentScore());
		}
		Score gameScore = this.getCurrentSet().getCurrentGame().getScore();

		return new MatchScore(new BestOf3ScoreInSets(setScores), gameScore);
	}

	public void restore(MatchMemento memento) throws IllegalMoveException {
		this.currentScore = memento.getScore();
		this.status = memento.getStatus();
		this.sets = new ArrayList<Set>();
		for (SetMemento setMemento : memento.getSetMementos()) {
			this.sets.add(Set.restore(setMemento));
		}
	}

	public MatchMemento saveToMemento() {
		List<SetMemento> setMementos = new ArrayList<SetMemento>();
		for (Set set : this.sets) {
			setMementos.add(set.saveToMemento());
		}
		CountableScore countableScore = new CountableScore(
				this.currentScore.getWon(), this.currentScore.getLost());
		return new MatchMemento(this.status, countableScore, setMementos
				);
	}

	public void fastForwardTo(ScoreInSets futureScore)
			throws IllegalMoveException {
		ScoreInSets currentScore = this.currentScore().getScoreInSets();
		try {
			if (!futureScore.canFollow(currentScore)) {
				throw new IllegalMoveException(futureScore + " can't follow "
						+ currentScore + ".");
			}
		} catch (IllegalScoreException e) {
			throw new IllegalMoveException(futureScore + " can't follow "
					+ currentScore + ".");
		}
		if (futureScore.size() > currentScore.size()) {
			// finish current set
			this.currentSet()
					.finishWith(futureScore.part(this.sets.size() - 1));
			this.updateStatus();
			// add the new sets
			List<Set> newSets = this.setFactory.fromScores(
					futureScore.partialScore(this.sets.size()),
					CountableScore.ZERO);
			for (Set set : newSets) {
				this.sets.add(set);
				this.updateStatus();
			}
		} else {
			this.currentSet().fastForwardTo(futureScore.lastPart());
			this.updateStatus();
		}
	}

	public void readyForTracking() throws IllegalStateException {
		if (player == null) {
			throw new IllegalStateException("Player undefined.");
		}
		if (opponent == null) {
			throw new IllegalStateException("Opponent undefined.");
		}
		if (surface == null) {
			throw new IllegalStateException("Court surface undefined.");
		}
		player.validate();
		opponent.validate();
	}

	public boolean atBeginningOfAGame() {
		return this.getCurrentSet().getCurrentGame().getScore()
				.equals(CountableScore.ZERO);
	}

	public void finishTracking() {
		this.trackingStatus = TrackingStatus.FINISHED;
	}

	public void resumeTracking() {
		this.trackingStatus = TrackingStatus.TRACKING;
	}

	public boolean isTracking() {
		return this.trackingStatus == TrackingStatus.TRACKING;
	}

	public boolean isFinished() {
		return this.trackingStatus == TrackingStatus.FINISHED;
	}

	public void setSetFactory(SetFactory setFactory) {
		this.setFactory = setFactory;
	}

	public CountableScore getCurrentScore() {
		return currentScore;
	}

	public WinStatus getStatus() {
		return status;
	}

	public List<Set> getSets() {
		return this.sets;
	}

	public Set getCurrentSet() {
		return this.currentSet();
	}

	public long getTime() {
		return time;
	}

	public Player getPlayer() {
		return player;
	}

	public void setPlayer(Player player) {
		this.player = player;
	}

	public Player getOpponent() {
		return opponent;
	}

	public void setOpponent(Player opponent) {
		this.opponent = opponent;
	}

	public boolean isServeFirst() {
		return serveFirst;
	}

	public void setServeFirst(boolean serveFirst) {
		this.serveFirst = serveFirst;
	}

	public TrackingStatus getTrackingStatus() {
		return trackingStatus;
	}

	public void setTrackingStatus(TrackingStatus trackingStatus) {
		this.trackingStatus = trackingStatus;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Surface getSurface() {
		return surface;
	}

	public void setSurface(Surface surface) {
		this.surface = surface;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	private Set currentSet() {
		return this.sets.get(this.sets.size() - 1);
	}

	private void updateStatus() {
		Set currentSet = this.currentSet();
		if (currentSet.getStatus() != WinStatus.PENDING) {
			this.currentScore = this.currentScore.count(currentSet.getStatus());
			int won = this.currentScore.getWon();
			int lost = this.currentScore.getLost();
			if (Math.max(won, lost) == 2) {
				// the match was decided
				this.status = won > lost ? WinStatus.WON : WinStatus.LOST;
			}
		}

	}

}
