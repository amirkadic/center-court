/*******************************************************************************
 * Copyright (c) 2012 Amir Kadić.
 * 
 * This file is part of Center Court.
 * 
 * Center Court is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Center Court is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Center Court.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Contributors:
 *     Amir Kadić - initial API and implementation
 ******************************************************************************/
package tennis.app.test;

import tennis.app.gui.activity.Tracking;
import tennis.app.R;
import android.test.ActivityInstrumentationTestCase2;
import android.widget.TextView;

import com.jayway.android.robotium.solo.Solo;


public class TrackingTest extends ActivityInstrumentationTestCase2<Tracking> {

	private Solo solo;

	public void setUp() throws Exception {
//		solo = new Solo(getInstrumentation(), getActivity());
//		solo.clickOnMenuItem("New match");
//		sleep(3);
//		solo.clickOnView(solo.getCurrentActivity().findViewById(R.id.playerNextButton));
//		sleep(3);
//		solo.clickOnView(solo.getCurrentActivity().findViewById(R.id.opponentNextButton));
//		sleep(3);
	}

	public TrackingTest() {
		super(Tracking.class);
	}

	public void testActivity() {
//		Tracking activity = getActivity();
//		assertNotNull(activity);
	}

	public void testWinButton() throws InterruptedException {
//		solo.clickOnButton("Won");
//		sleep(3);
//		assertEquals("15:0", gameScoreText());
//		assertEquals("0:0", setScoresText());
	}

	private String gameScoreText() {
		return String.valueOf(((TextView) getActivity()
				.findViewById(R.id.gameScoreTextView)).getText());
	}
	private String setScoresText() {
		return String.valueOf(((TextView) getActivity()
				.findViewById(R.id.setScoresTextView)).getText());
	}
	
	private void sleep(int seconds) {
		try {
			Thread.sleep(1000*seconds);
		} catch (InterruptedException e) {
			fail("Test thread interrupted.");
		}
	}
}
