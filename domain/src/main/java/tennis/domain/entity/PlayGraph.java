/*******************************************************************************
 * Copyright (c) 2012 Amir Kadić.
 * 
 * This file is part of Center Court.
 * 
 * Center Court is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Center Court is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Center Court.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Contributors:
 *     Amir Kadić - initial API and implementation
 ******************************************************************************/
package tennis.domain.entity;

import tennis.domain.IllegalMoveException;
import tennis.domain.WinStatus;

public class PlayGraph {

	private PlayElement currentPlayElement;

	public PlayGraph() {
		// for XStream compatibility only
	}
	
	public PlayGraph(PlayElement firstPlayElement) {
		this.currentPlayElement = firstPlayElement;
	}

	public void move(WinStatus winStatus) throws IllegalMoveException {

		PlayElement nextPlayElement = null;

		if (WinStatus.WON == winStatus) {
			nextPlayElement = this.currentPlayElement.getOnWin();
		} else if (WinStatus.LOST == winStatus) {
			nextPlayElement = this.currentPlayElement.getOnLoss();
		} else {
			throw new IllegalMoveException(
					"Can't move if nothing was won or lost.");
		}

		if (nextPlayElement == null) {
			throw new IllegalMoveException("This period has been "
					+ this.currentPlayElement.getWinStatus() + " already.");
		}
		this.currentPlayElement = nextPlayElement;
	}

	
	public PlayElement currentPlayElement() {
		return this.currentPlayElement;
	}

	public WinStatus currentWinStatus() {
		return this.currentPlayElement.getWinStatus();
	}


	
}
