/*******************************************************************************
 * Copyright (c) 2012 Amir Kadić.
 * 
 * This file is part of Center Court.
 * 
 * Center Court is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Center Court is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Center Court.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Contributors:
 *     Amir Kadić - initial API and implementation
 ******************************************************************************/
package tennis.domain.entity;

import java.util.Date;
import java.util.List;

import tennis.domain.IllegalMoveException;
import tennis.domain.PlayGraphFactory;
import tennis.domain.WinStatus;
import tennis.domain.memento.GameMemento;
import tennis.domain.memento.RegularGameMemento;

public class RegularGame extends Game {

	private PlayGraphFactory playGraphFactory;
	private PlayGraph playGraph;
	private Score score;

	public RegularGame() {

	}

	/**
	 * For restoration purposes.
	 * 
	 * @throws IllegalMoveException
	 */
	public RegularGame(long id, WinStatus status, List<Point> points,
			Date time, boolean serve, Score score,
			PlayGraphFactory playGraphFactory) throws IllegalMoveException {
		this.id = id;
		this.status = status;
		this.time = time;
		this.serve = serve;
		this.score = score;
		this.playGraphFactory = playGraphFactory;
		if (this.hasStarted()) {
			this.playGraph = this.playGraphFactory.fromScore(this.score);
		}
	}

	public void start() {
		this.playGraph = this.playGraphFactory.newGameGraph();
		this.status = this.playGraph.currentWinStatus();
		this.time = new Date();
		this.score = this.playGraph.currentPlayElement().getScore();
	}

	public void add(Point point) throws IllegalMoveException {
		this.playGraph.move(point.getStatus());
		this.status = this.playGraph.currentWinStatus();
		this.points.add(point);
		this.score = this.playGraph.currentPlayElement().getScore();
		// how cool is this? should the point know what game score
		// it produced? this can be kept in a separate "point2Score"
		// map so the point is kept clean.
		point.setScore(this.score);
	}

	public void setPlayGraphFactory(PlayGraphFactory playGraphFactory) {
		this.playGraphFactory = playGraphFactory;
	}

	public GameMemento saveToMemento() {
		return new RegularGameMemento(this.status,
				this.playGraph.currentPlayElement(), this.points,
				this.playGraphFactory, this.time, this.serve);
	}

	public static RegularGame restore(RegularGameMemento memento)
			throws IllegalMoveException {
		RegularGame game = new RegularGame();
		game.playGraph = memento.getPlayGraphFactory().fromScore(
				memento.getPlayElement().getScore());
		game.playGraphFactory = memento.getPlayGraphFactory();
		game.points = memento.getPoints();
		game.status = memento.getStatus();
		game.time = memento.getTime();
		game.serve = memento.isServe();
		game.score = memento.getPlayElement().getScore();
		return game;
	}

	public boolean hasStarted() {
		return this.time != null;
	}

	@Override
	public Score getScore() {
		return this.score;
	}

}
